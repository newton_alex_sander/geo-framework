from setuptools import setup
from babel.messages import frontend as babel
setup(name='geo',
      namespace_packages=['geo'],
      version='1.0',
      packages = ['geo', 'geo.events', 'geo.validation', 
                  'geo.lib', 'geo.database', 'geo.testers', 
                  'geo.geo_functions', 'geo.server', 'geo.tools', 
                  'geo.controllers', 'geo.templates', 'geo.static'],
      include_package_data=True,
      zip_safe=False,
      author="Newton Alex Sander",
      author_email="sandernewtonsander@gmail.com",
      cmdclass = {'compile_catalog': babel.compile_catalog,
                'extract_messages': babel.extract_messages,
                'init_catalog': babel.init_catalog,
                'update_catalog': babel.update_catalog},
      message_extractors={'geo': [
            ('**.py', 'python', None),
            ('templates/**.html', 'genshi', None),
            ('static/**', 'ignore', None)]},
      install_requires=['cherrypy >= 3.2.2',
                        'Formencode',
                        'Genshi',
                        'SQLAlchemy',
                        'Geoalchemy',
                        'shapely',
                        'restclient',
                        'minixsv',
                        'psycopg2'],
      
      package_data={'templates': ['*.html'],
                    'static':['*/*/*/*']},
      )

# 
#apt-get install postgresql postgresql-8.4-postgis python-setuptools python-dev libpq-dev python-psycopg2
#su root
#su postgres
#First, create a role that will own the tables within the template database:
#psql -c "CREATE ROLE gisgroup;"
#
#Second, create and populate the template database:
#
#createdb -E UNICODE template_postgis
#createlang -d template_postgis plpgsql
#psql -d template_postgis < /usr/share/postgresql/8.4/contrib/postgis-1.5/postgis.sql
#psql -d template_postgis < /usr/share/postgresql/8.4/contrib/postgis-1.5/spatial_ref_sys.sql
#psql -d template_postgis < /usr/share/postgresql/8.4/contrib/postgis_comments.sql
#
#Third, set the ownership to the role you created:
#
#psql -c "ALTER TABLE geometry_columns OWNER TO gisgroup;" template_postgis
#psql -c "ALTER TABLE spatial_ref_sys OWNER TO gisgroup;" template_postgis
#
#Fourth, we create the user for our database:
#
#psql -c "CREATE USER yourgisuser WITH PASSWORD 'yourpassword';"
#psql -c "GRANT gisgroup TO yourgisuser;"
#
#Fifth, and last, we create a new postgis enable database:
#createdb -T template_postgis -O yourgisuser your_database_name 



#  
# python setup.py update_catalog -l pt -i geo/i18n/translate.pot -o geo/i18n/pt/LC_MESSAGES/translate.po 
# python setup.py compile_catalog -d geo/i18n/ --locale pt -D translate