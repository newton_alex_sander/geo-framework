function modal_text_area(text_field, title) {
	var text_area = $('<textarea id="editor" name="editor" style="width : 500px;">{0}</textarea>'.format(text_field.value) );
	var div = $('<div id="dialog" name="dialog" title="{0}"></div>'.format(title)).html(text_area);
	$( div ).dialog(
		{
		 modal:true, 
		 width: 530,
		 close: function(event, ui) 
		 {
		     text_field.value= $("#editor").val();
		     $("#dialog").remove();
		 	}	
		});
};
