var map; 					// GOOGLE MAPS OBJECT 
var source;					// EVENT SOURCE OBJECT

var points = new Object();
var areas =  new Object();
var area_events=new Object();
var point_events=new Object();
var first_point = false;

function manipulate_event(data){
	// IT SAVES THE EVENT IN LOCAL OBJECTS, CREATE AN
	// NOTIFICATION IF THIS IS THE FIRST TIME EVENT.ID COMES
	// OR SETTYPE OF NOTY IF IS NOT THE FIRST TIME
	var event = jQuery.parseJSON(data); 
	if (event.type=='Area'){
		if (area_events.hasOwnProperty(event.id)){
			if (event.active!="true"){
				area_events[event.id].setType("alert");
			}
			if (event.active=="true"){
				area_events[event.id].setType("success");
			}
		}
		else{
			area_events[event.id]=add_noty(event);
		}
	}
	if (event.type=='Point'){
		if (point_events.hasOwnProperty(event.id)){
			if (event.active!="true"){
				point_events[event.id].setType("alert");
			}
			if (event.active=="true"){
				point_events[event.id].setType("success");
			}
		}
		else{
			point_events[event.id]=add_noty(event);
		}
	}
}

function add_noty(event, event_type){
	// CREATE THE NOTIFICATION OF AN EVENT,
	// RETURN THE NOTIFICATION OBJECT SO WE CAN CONTROL IT
	var n = noty({
	  		text: event.msg,
	  		type: "success",
	  		force: true, 
	        dismissQueue: true,
	        closeWith: ['button'],
	  		layout: 'topRight',
	  		theme: 'default',
	  		buttons: [
		        {addClass: 'btn btn-primary', text: messages.eventInfo, onClick: function() {
		            get_more_event_info(event.id, event.type);
		          }
		        },
		        {addClass: 'btn btn-danger', text: messages.close, onClick: function( noty_object ) {
			        remove_event(event.id, event.type);
			        noty_object.close();
			      }
			    }
		    ]
	  	});
	return n;
}

function remove_event(event_id, event_type){
	// WHEN EVENT CLOSE IS CLICKED, REMOVE EVENT
  	if (event_type=='Area'){
		if (area_events.hasOwnProperty(event_id)){
			delete area_events[event_id];
		}
	}
	if (event_type=='Point'){
		if (point_events.hasOwnProperty(event_id)){
		 	delete point_events[event_id];
		}
	}
}

function get_more_event_info(event_id, event_type){
	$.ajax({
		  url: "event_info",
		  data: {
			  item_id: event_id,
			  item_type:event_type,
			  },
		  success: function(data) {
			  var title = messages.eventInfo;
			  $( '<div id="dialog" title="{0}"> {1}  </div>'.format(title, data) ).dialog();
			  }
		});
       
}

function manipulate_marker(data){
	// SAVE MARKER IN A LOCAL OBJECT
    var server_marker = jQuery.parseJSON(data);
    var device = server_marker.device_id;
    var device_description = server_marker.device_description;
    var point = new google.maps.LatLng(server_marker.lat, server_marker.lon);
    var image = new google.maps.MarkerImage('/media/images/stickman.png');
   	var marker = new google.maps.Marker({
   	        position: point, 
   	        map: map,
   	        icon:image,
   	        title:device_description,
   	        });
   	if (first_point==false){
   		first_point = true;
   		map.setCenter(point);
   	}
   	if (!points.hasOwnProperty(device)){
   		points[device] = Array();
   	}
   	var device_points = points[device];
	if(device_points.length>10){
   		var almost_dead_point=device_points.shift();
   		almost_dead_point.setMap(null);
	}
	device_points[device_points.length] = marker;
	for(i = 0; i < device_points.length-1; i++) {
		point = device_points[i];
		point.setIcon(); 
	}
   	
   	
   		
}

function resizeMap () {
	// MAP WILL USE ALL WIDTH AND WINDOW HEIGHT - 190
    $('#map_canvas').height($(window).height()-190); 
}

function insert_area_in_map(points, area_id){
	// RECEIVES AN ARRAY OF POINTS AND AN ID
	// WILL ADD THOSE POINTS IN A POLYGON AND 
	// SAVE AREA OBJECT IN A LOCAL ARRAY
	var mapArray = new Array(points.length);
	for(i = 0; i < points.length; i++) {
		point = points[i];
		mapArray[i]= new google.maps.LatLng(point[0], point[1]); 
	}
	areaMap = new google.maps.Polygon({
		paths: mapArray,
		strokeColor: '#FF0000',
		strokeOpacity: 0.8,
		strokeWeight: 3,
		fillColor: '#FF0000',
		fillOpacity: 0.35,
		clickable:false,
		});
	areaMap.setMap(map);
	map.setCenter(mapArray[0]);
	map.setZoom(14);
	areas[area_id] = areaMap;
			
}

function click_listener(type, checked, objectId){
	// WHEN CLICKED ON MULTISELECTS (DEVICE/AREA), AJAX CALLS
	var url = "";
	if(type=="device"){
		if(checked){
	    	url = "add_listener";
		}
		else{
			url = "del_listener";
		}
		$.ajax({
			  url: url,
			  data: {device_id : objectId},
			  success: function(data) {
				    //alert(data);
				  }
			});
	}
	else if(type=="area"){
		if(checked){
	    	url = "get_area";
			$.ajax({
				  url: url,
				  dataType: 'json',
				  data: {area_id : objectId},
				  success: function(data) {
					   	if(data!="False"){
					   		insert_area_in_map(data, objectId);
					  }
				  }
			});
		}
		else{
			//remove area
			areas[objectId].setMap(null);
		}
	}
}

function start_streaming() {
	// CREATE EVENT SOURCE SOCKET, BIND MARKER AND EVENT LISTENERS
	if (source==null){
        source = new EventSource('device_data');
        source.addEventListener('marker', 
                   function(receivedObject){
                       manipulate_marker(receivedObject.data);
                   }, false);
        source.addEventListener('event', 
                   function(receivedObject){
                       manipulate_event(receivedObject.data);
                   }, false);
        //alert("Streaming started!");
    }
    else{
    	add_noty(messages.alreadyStreaming, "error");
    }
}

function stop_streaming(){
	// CLOUSE EVENT SOURCE SOCKET, TELL SERVER TOO
	source.close();
	source = null;
	$.ajax({
	  url: 'stop_streaming',
	  success: function(data) {
		    //alert(data);
		  }
	});
}


function create_widgets(){
	// STREAMING / NOT STREAMING BUTTONSET
	$( "#radio" ).buttonset();
	$( "#radio" ).change(function () {
		var selection=$("#radio :radio:checked").attr('id');
		if(selection=="start"){
			start_streaming();
		}
		else{
			stop_streaming();
		}
	});
	
	// MULTISELECT SELECT DEVICES
	$("#device_multiselect")
	   .multiselect({
	   	  header:false,
	      noneSelectedText: messages.selectDevices,
	      selectedList: 0,
	   })
	   .bind("multiselectclick", function(event, ui){
	   		click_listener('device', ui.checked, ui.value)
		});
	$("#device_multiselect").multiselect("uncheckAll");
	$("#device_multiselect").multiselect("refresh");
	// MULTISELECT SELECT AREAS
	$("#area_multiselect")
	   .multiselect({
	      header:false,
	      noneSelectedText: messages.visualizeArea,
	      selectedList: 0,
	   })
	   .bind("multiselectclick", function(event, ui){
	   		click_listener('area', ui.checked, ui.value)
		});
	$("#area_multiselect").multiselect("uncheckAll");
	$("#area_multiselect").multiselect("refresh");

}
		
function initialize() {
	// CREATE GOOGLE MAPS AND RESIZE IT
	var myLatlng = new google.maps.LatLng(48.97920000, 8.41220000);
	var myOptions = {
		zoom: 14,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.HYBRID 
	}
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	resizeMap(); 
	create_widgets();
}
