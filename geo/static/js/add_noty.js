function add_noty(data, noty_type){
	noty_type = typeof noty_type !== 'undefined' ? noty_type : "success";
	var n = noty({
	  		text: data,
	  		type: noty_type,
	  		force: true, 
	        dismissQueue: true,
	  		layout: 'topRight',
	  		theme: 'default',
	  	});
}