		function set_point_config(deviceId, functionId){
				var config_object = ["#config-", deviceId, "-", functionId].join("")
				var config_value =  $(config_object).val();
				$.ajax({
				  url: "set_config",
				  data: {device_id : deviceId,
					  	function_id: functionId,
					  	config:config_value},
				  success: function(data) {
					    add_noty(data, "alert");
					  }
				});
			}


		
		
    	function set_area_config(deviceId, areaId, functionId){
    		var config_object = ["#config-", deviceId, "-", areaId, "-", functionId].join("")
    		console.log(config_object);
			var config_value =  $(config_object).val();

	    	$.ajax({
			  url: "set_config",
			  data: {device_id : deviceId,
				  	function_id: functionId,
				  	area_id: areaId,
				  	config:config_value},
			  success: function(data) {
				    add_noty(data, "alert");
				  }
			});
    	}
    	
    	
    	function get_help(functionId){
	    	$.ajax({
			  url: "get_help",
			  data: {
				  	function_id: functionId},
			  success: function(data) {
				  var title = messages.populatingConfig;
				  $( '<div id="dialog" title="{0}"> {1}  </div>'.format(title, data) ).dialog();
				  }
			});
    	}
    	
    	function remove_point_function(deviceId, functionId){

	    	$.ajax({
			  url: "remove",
			  data: {
				  	device_id: deviceId,
				  	function_id: functionId
		  },
			  success: function(data) {
				  if(data=="True"){
				  	add_noty(messages.removed);
				  	var config_object = ["#config-", deviceId, "-", functionId].join("")
				  	$(config_object).val("");;
				  }
				  else{
					  add_noty(data, "alert");
				  }
			  }
			});
    	}
    	
    	function remove_area_function(deviceId, areaId, functionId){

	    	$.ajax({
			  url: "remove",
			  data: {
				  	device_id: deviceId,
				  	function_id: functionId,
				  	area_id: areaId},
			  success: function(data) {
				  if(data=="True"){
				  	add_noty(messages.removed);
				  	var config_object = ["#config-", deviceId, "-", areaId, "-", functionId].join("")
				  	$(config_object).val("");;
				  }
				  else{
					  add_noty(data, "alert");
				  }
			  }
			});
    	}
    	