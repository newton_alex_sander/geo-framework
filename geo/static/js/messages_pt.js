messages = {
		removed:"Removido",
		populatingConfig:"Parâmetros de configuração",
		saved:"Salvo!",
		eventInfo:"Evento",
		close:"Fechar",
		alreadyStreaming:"Já monitorando",
		selectDevices:"Dispositivos para monitorar",
		visualizeArea:"Visualizar área no mapa",
		selected:"# selecionado(s)",
	}