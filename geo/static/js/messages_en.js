messages = {
		removed:"Removed",
		populatingConfig:"Populating config parameters",
		saved:"Saved!",
		eventInfo:"Event Info",
		close:"Close",
		alreadyStreaming:"Already streaming",
		selectDevices:"Select devices to monitor",
		visualizeArea:"Visualize area on map",
		selected:"# selected",
	}