function modal_cleditor(text_field, title) {
	editor=null;
	var text_area = $('<textarea id="editor" name="editor">{0}</textarea>'.format(text_field.value) );
	var div = $('<div id="dialog" name="dialog" title="{0}"></div>'.format(title)).html(text_area);
	$( div ).dialog(
		{
		 modal:true, 
		 width: 530,
		 open: function(event, ui) {
			 editor = $("#editor").cleditor()[0];
		 },
		 buttons: {
             Save: function() {
            	 text_field.value= editor.$area.val();
            	 add_noty(messages.saved);
             },
             Close:function() {
                 div.remove();
             } 
		 },
		 close: function(event, ui) 
		 {
		     div.remove();
		 	}	
		});
};
