'''
Created on Feb 13, 2012

@author: newton
'''
from geo.database.sqlalchemy_tool import SAEnginePlugin
import cherrypy
from cherrypy.process import plugins
from Queue import Queue
from geo.database.model import AreaEvent, PointEvent

class SessionQueue:
    session_id = None
    listening_devices_list=[]
    stream_queue = None
    def __init__(self, session_id):
        self.session_id = session_id
        self.stream_queue = Queue()
    
    def add_listener_device(self, device_id):
        if device_id not in self.listening_devices_list:
            self.listening_devices_list.append(device_id)
            return True
        return False
        
    def del_listener_device(self, device_id):
        if device_id in self.listening_devices_list:
            self.listening_devices_list.remove(device_id)
            return True
        return False
    
    def clear_listening_devices(self):
        self.listening_devices_list = []
        
        while not self.stream_queue.empty():
            self.stream_queue.get_nowait()
            self.stream_queue.task_done()
        
    def add_item_to_queue(self, item):
        self.stream_queue.put_nowait(item)
    
    def get_item_from_queue(self):
        item = self.stream_queue.get_nowait()
        self.stream_queue.task_done()
        return item
    
    def get_stream_queue_size(self):
        return self.stream_queue.qsize()
    
    def stream_queue_empty(self):
        return self.stream_queue.empty()
    

            
    

class ServerDB(plugins.SimplePlugin):
    """
        Ideia = {"device_id":{"ultimo_ponto":point}}
    """
    
    __data = {}
    __session = None
    session_queues={}
    def set_data(self, device_id, key, value):
        if self.__data.get(device_id) ==None:
            self.__data[device_id]={}
        self.__data[device_id][key]= value
            
    def get_data(self, device_id, key):
        if self.__data.get(device_id):
            return self.__data.get(device_id).get(key)
        else:
            return None
        
    def has_key(self, device_id, key):
        if self.__data.get(device_id):
            return self.__data.get(device_id).has_key(key)
    
    def remove_key(self, device_id, key):
        if self.has_key(device_id, key):
            self.__data.get(device_id).pop(key)

    def start(self):
        self.__session = SAEnginePlugin.get_session()
        
    def stop(self):
        self.session_queues= None

    def create_session_queue(self, session_id):
        if not self.session_queues.has_key(session_id):
            self.session_queues[session_id] = SessionQueue(session_id)
        return self.session_queues.get(session_id)
            
    def add_listener(self, session_id, device_id):
        if not self.session_queues.has_key(session_id):
            self.create_session_queue(session_id)
        self.session_queues[session_id].add_listener_device(device_id)
        
    def get_session_queue(self, session_id):
        return self.session_queues.get(session_id)
    
    
    def insert_into_queues(self, device_id, data):
        for session_queue_object in self.session_queues.itervalues():
            if device_id in session_queue_object.listening_devices_list:
                session_queue_object.add_item_to_queue(data)
            
    