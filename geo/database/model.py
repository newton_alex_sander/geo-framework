'''
Created on Jan 23, 2012

@author: newton
'''
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Unicode, String, DateTime, BOOLEAN, PickleType, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.associationproxy import association_proxy

import geoalchemy
from geoalchemy import WKTSpatialElement

from datetime import datetime

Base = declarative_base()
   
class Point(Base):
    __tablename__ = 'points'
    id = Column(Integer, primary_key=True)
    device_id = Column(Integer, ForeignKey('devices.id'))
    timestamp = Column(DateTime, default=datetime.now)
    geo_point = geoalchemy.GeometryColumn(geoalchemy.Point(2))
    device_timestamp = Column(DateTime, default=datetime.now)
    
    device = relationship("Device",backref=backref("device_points",
                                cascade="all, delete-orphan"))

    def __init__(self, device, lati, longi, timestamp=None, device_timestamp=None):
        self.device = device
        self.timestamp = timestamp
        self.device_timestamp = device_timestamp
        self.geo_point = WKTSpatialElement("POINT(%s %s)" %(lati, longi))

    def __repr__(self):
        return "<Point('%s','%s', '%s')>" % (self.device_id, self.timestamp, self.device_timestamp)
   
   
class Area(Base):
    __tablename__ = 'areas'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode, nullable=False)
    created = Column(DateTime, default=datetime.now())
    geom = geoalchemy.GeometryColumn(geoalchemy.Polygon(2))
    __table_args__ = (
        UniqueConstraint("name"),
    )


class Device(Base):
    __tablename__ = 'devices'
    id = Column(Integer, primary_key=True)
    device_id = Column(String, nullable=False, unique=True)
    description = Column(String, nullable=True)
    point_functions_applied = association_proxy('device_point_functions', 'function')
    area_functions_applied = association_proxy('device_area_functions', 'function')

    def __init__(self, device_id, description):
        self.device_id = device_id
        self.description = description

    def __repr__(self):
        return "%s" % (self.description)
   

class Function(Base):
    __tablename__ = 'functions'
    id = Column(Integer, primary_key=True)
    constant = Column(String, nullable=False, unique=True)
    description = Column(String, nullable=True)
    area_function = Column(BOOLEAN, nullable=False)
    user_help = Column(String, nullable=True)
    def __init__(self, constant, description, area_function='off', user_help=None,):
        self.constant = constant
        self.description = description
        self.area_function = area_function
        if user_help:
            self.user_help = user_help

    def __repr__(self):
        return "%s" % (self.constant)
   

class DevicePointFunction(Base):
    __tablename__ = 'device_point_functions'
    #===========================================================================
    # * tabela com associacao entre os devices e as funcoes aplicadas
    #===========================================================================
    id = Column(Integer, primary_key=True)
    function_id = Column(Integer,  ForeignKey('functions.id'), nullable=False)
    device_id = Column(Integer, ForeignKey('devices.id'), nullable=False)
    parameters = Column(PickleType, nullable=True)
    device = relationship(Device,
                backref=backref("device_point_functions",
                                cascade="all, delete-orphan")
            )

    # reference to the "Keyword" object
    function = relationship("Function",
                            backref=backref("point_functions",
                                cascade="all, delete-orphan"))
    
    __table_args__ = (
        UniqueConstraint("function_id", "device_id"),
    )




    def __init__(self, function, device, parameters=None):
        self.function = function
        self.device = device
        self.parameters = parameters

    def __repr__(self):
        return "<DevicePointFunction('%s','%s', '%s')>" % (self.id, self.function.constant, self.device)


class DeviceAreaFunction(Base):
    __tablename__ = 'device_area_functions'
    #===========================================================================
    # * tabela com associacao entre os devices, funcoes aplicadas e areas
    #===========================================================================
    id = Column(Integer, primary_key=True)
    function_id = Column(Integer,  ForeignKey('functions.id'), nullable=False)
    device_id = Column(Integer, ForeignKey('devices.id'), nullable=False)
    area_id = Column(Integer, ForeignKey('areas.id'), nullable=False)
    parameters = Column(PickleType, nullable=True)
    device = relationship(Device,
                backref=backref("device_area_functions",
                                cascade="all, delete-orphan")
            )

    # reference to the "Keyword" object
    function = relationship("Function",
                            backref=backref("device_area_functions",
                                cascade="all, delete-orphan"))
    area = relationship("Area")
    
    __table_args__ = (
        UniqueConstraint("function_id", "device_id", "area_id"),
    )



    def __init__(self, function, device, area, parameters=None):
        self.function = function
        self.device = device
        self.area = area
        self.parameters = parameters

    def __repr__(self):
        return "<DevicePointFunction('%s','%s', '%s')>" % (self.id, self.function.constant, self.device)

class PointEvent(Base):
    __tablename__ = 'point_events'
    id = Column(Integer, primary_key=True)
    device_point_func_id = Column(Integer,  ForeignKey('device_point_functions.id'), nullable=False)
    event_started = Column(DateTime, default=datetime.now)
    event_stopped = Column(DateTime)
    active = Column(BOOLEAN, default=True)
    alerting = Column(BOOLEAN, default=True)
    message = Column(String, nullable=True)
    
    device_point_func = relationship(DevicePointFunction,
                backref=backref("point_events",
                                cascade="all, delete-orphan")
            )
    
    __table_args__ = (
        UniqueConstraint("device_point_func_id", "event_started"),
    )




    def __init__(self, device_point_func, message, event_started=None, event_stopped=None, active=True, alerting=True):
        self.device_point_func = device_point_func
        self.message = message
        self.event_started = event_started
        self.event_stopped = event_stopped
        self.active = active
        self.alerting = alerting

    def __repr__(self):
        return "<PointEvent('%s','%s','%s', '%s')>" % (self.id, self.message, self.device_point_func.function.constant, self.event_started)
   

class AreaEvent(Base):
    __tablename__ = 'area_events'
    id = Column(Integer, primary_key=True)
    device_area_func_id = Column(Integer,  ForeignKey('device_area_functions.id'), nullable=False)
    event_started = Column(DateTime, default=datetime.now)
    event_stopped = Column(DateTime)
    active = Column(BOOLEAN, default=True)
    alerting = Column(BOOLEAN, default=True)
    message = Column(String, nullable=True)
    
    device_area_func = relationship(DeviceAreaFunction,
                backref=backref("area_events",
                                cascade="all, delete-orphan")
            )
    
    __table_args__ = (
        UniqueConstraint("device_area_func_id","event_started"),
    )

    def __init__(self, device_area_func, message, event_started=None, event_stopped=None, active=True, alerting=True):
        self.device_area_func = device_area_func
        self.message = message
        self.event_started = event_started
        self.event_stopped = event_stopped
        self.active = active
        self.alerting = alerting

    def __repr__(self):
        return "<AreaEvent('%s','%s','%s', '%s')>" % (self.id, self.message, self.device_area_func.function.constant, self.event_started)
   

geoalchemy.GeometryDDL(Point.__table__)
geoalchemy.GeometryDDL(Area.__table__) 