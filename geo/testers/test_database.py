from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from geo.server import get_config, set_framework_config

def test_database_connection(config):
    """
        Only try to connect to database
    """
    set_framework_config(config)
    sqlalchemy_engine_address = get_config('sqlalchemy_engine_address')
    engine = create_engine(sqlalchemy_engine_address)
    try:
        engine.connect()
        print "Database connection was successful!"
    except OperationalError, e:
        print "Can not connect do Database. Error data: %s", e
    except Exception, e:
        print "Error! Error data: %s", e