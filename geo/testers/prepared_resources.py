from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from geo.database import Function
from geo.server import get_config

sqlalchemy_engine_address = get_config('sqlalchemy_engine_address')
def create_database_data():
    """
        Will create the geo_functions provided by the framework
        in the database:
            SPEED
            INTERSECTS_AREA
            NOT_INTERSECTS_AREA
    """
    engine = create_engine(sqlalchemy_engine_address)
    Session = sessionmaker(bind=engine)
    session = Session()
    
    speed = Function(constant='SPEED', description='Speed verification', area_function='off', 
        user_help="""<b><i>max = </i></b>Event when speed exceed value(km/h). Ex.:<font color="#3366ff"><b>max=23;</b></font><br><b><i>min = </i></b>Event when speed is bellow value(km/h). Ex.:<font color="#3366ff"><b>min=50;</b></font><b><br></b>""")
    session.add(speed)
    session.commit()
    
    intersects_area = Function(constant='INTERSECTS_AREA', description='Generates event when object is inside area', area_function='on', 
        user_help="""<i><b>intersects</b></i> = event when object enters area. Ex.:<font color="#3366ff"><b>intersects=True;</b></font><br><i><b>time_inside = </b></i>max<i><b> </b></i>value(seconds) inside area. Ex.:<font color="#3366ff"><b>time_inside=60;</b></font><br><i><b>max_speed =&nbsp;</b></i>max value(km/h) inside area. Ex.:<font color="#3366ff"><b>max_speed=80;</b></font><br><i><b>min_speed = </b></i>min value(km/h) inside area. Ex.:<font color="#3366ff"><b>min_speed=20;</b></font>""")
    session.add(intersects_area)
    session.commit()
    
    not_intersects_area = Function(constant='NOT_INTERSECTS_AREA', description='Generates event when object is not inside area', area_function='on', 
        user_help="""<i><b>not_</b></i><i><b>intersects</b></i> = event when object exits area. Ex.:<font color="#3366ff"><b>not_intersects=True;</b></font><br><i><b>time_outside = </b></i>max<i><b> </b></i>value(seconds) outside area. Ex.:<font color="#3366ff"><b>time_outside=60;</b></font><br><br><b></b>""")
    session.add(not_intersects_area)
    session.commit()