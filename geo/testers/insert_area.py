from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from geoalchemy import WKTSpatialElement
from geo.database import Area
from geo.server import get_config, set_framework_config


def insert_area(config):
    """
        Insert a predefined Area in database
    """
    set_framework_config(config)
    sqlalchemy_engine_address = get_config('sqlalchemy_engine_address')
    engine = create_engine(sqlalchemy_engine_address)
    Session = sessionmaker(bind=engine)
    session = Session()
    
    
    wkt_lake1 = "POLYGON((48.983227 8.425452, \
                          48.984892 8.42557, \
                          48.985118 8.432007, \
                          48.983991 8.432951, \
                          48.982921 8.432093, \
                          48.983653 8.428059,\
                          48.983227 8.425452))"
    #wkt_lake1 = "POLYGON((-81.3 37.2, -80.63 38.04, -80.02 37.49, -81.3 37.2))"
    lake1 = Area(name="Lake Karlsruhe", geom=WKTSpatialElement(wkt_lake1))
    session.add(lake1)
    session.commit()