import time
from restclient import rest_invoke
from geo.server import get_config, set_framework_config

def insert_points(config):
    """
        Insert points in Database without stop
    """
    set_framework_config(config)
    server_ip_address = get_config('server_ip_address')
    server_http_port = get_config('server_http_port')
    base_url = "http://%s:%s/rest" % (server_ip_address, server_http_port)
    device_id = "353655044528747"
    
    base_lati = 48.979935
    base_longi = 8.426771
    increasing = 0.0001500
    counter = 0
    
    while True:
        
        # this code will make points go back and forth
        if counter == 50:
            increasing *= -1
            counter = 0
        counter += 1
        try:
            base_lati += increasing
            base_longi += increasing
            longi = base_longi.__str__()
            lati = base_lati.__str__()
            device_time = time.time().__str__()
            params = {"time":device_time, "longi":longi,"lati":lati, "deviceid":device_id}
            rest_invoke(base_url, method="POST",async=False, resp=True, params=params)
            time.sleep(1)
        except:
            time.sleep(1)
            print 'problem inserting point'