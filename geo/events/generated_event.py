from geo.tools.i18n_tool import ugettext as _

class GenericEvent(Exception):
    """
        GenericEvent is an Exception raised when a geo_function 
        find the right value.
        All raises of geo_functions need to extend this class 
    """
    message = None


#===============================================================================
# POINT EVENTS
#===============================================================================
class MaxSpeedEvent(GenericEvent):
    def __init__(self, speed):
        self.speed = speed
        self.message = _("Exceeded the speed limit: %.2fkm/h") % speed

class MinSpeedEvent(GenericEvent):
    def __init__(self, speed):
        self.speed = speed
        self.message = _("Speed below the minimum: %.2fkm/h") % speed
        

#===============================================================================
# AREA EVENTS
#===============================================================================
class Intersects(GenericEvent):
    def __init__(self, area_name):
        self.area_name = area_name
        self.message = _("Intersects area: %s" % area_name)

class MaxTimeInside(GenericEvent):
    def __init__(self, area_name, max_time, time_inside):
        self.area_name = area_name
        self.max_time = max_time
        self.time_inside = time_inside
        self.message = _("Intersected area: %s for too much time: %ss, max time was %ss") % (area_name, time_inside, max_time)

class MaxSpeedInside(GenericEvent):
    def __init__(self, area_name, speed_limit, speed):
        self.area_name = area_name
        self.speed_limit = speed_limit
        self.speed = speed
        self.message = _("Passed speed limit in area: %s,  %.2fkm/h, speed limit was %ss") % (area_name, speed, speed_limit)

class MinSpeedInside(GenericEvent):
    def __init__(self, area_name, speed_limit, speed):
        self.area_name = area_name
        self.speed_limit = speed_limit
        self.speed = speed
        self.message = _("Bellow speed limit in area: %s,  %.2fkm/h, speed limit was %ss")  % (area_name, speed, speed_limit)

class MaxTimeOutside(GenericEvent):
    def __init__(self, area_name, max_time, time_inside):
        self.area_name = area_name
        self.max_time = max_time
        self.time_inside = time_inside
        self.message = _("Intersected area: %s for too much time: %ss, max time was %ss") % (area_name, time_inside, max_time)
class NotIntersects(GenericEvent):
    def __init__(self, area_name):
        self.area_name = area_name
        self.message = _("Not intersecting area: %s")  % area_name