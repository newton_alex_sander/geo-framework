'''
Created on Feb 5, 2012

@author: newton
'''
from formencode import Schema, validators, FancyValidator, Invalid

class MyString(FancyValidator):
    messages = dict(
        not_a_string='Enter only letters, numbers, or _ (underscore)')
    def validate_other(self, value, state):
        try:
            str(value)
        except:
            raise Invalid(
                self.message('not_a_string', state), value, state)
            
            
class DefaultValidator(Schema):
    allow_extra_fields = True


class DeviceForm(Schema):
    """
        Device needs to have device_id and description
    """
    allow_extra_fields = True
    device_id = validators.UnicodeString(not_empty=True)
    description = MyString(not_empty=True)
    

class FunctionForm(Schema):
    """
        Function needs to have constant
    """
    allow_extra_fields = True
    constant = validators.UnicodeString(not_empty=True)
