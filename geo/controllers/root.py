'''
Created on Feb 1, 2012

@author: newton
'''
import cherrypy
from geo.lib import template
from geo.tools.i18n_tool import ugettext as _
class Root:
    """
        By default, will output root.html from geo lib templates.
        This is the first file that you should edit to extend
        this framework
    """
    @cherrypy.expose
    @template.output('root.html', geo_template=True)
    def index(self):
        return template.render(title=_("Welcome to your Geo Application"), geo_template=True)