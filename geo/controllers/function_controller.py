import cherrypy
from genshi.template import MarkupTemplate
from formencode import Invalid
from genshi.filters import HTMLFormFiller
from sqlalchemy.exc import IntegrityError

from default_controller import DefaultModelController
from geo.validation.form import FunctionForm
from geo.lib import template, html
from geo.database.model import Function
from geo.tools.i18n_tool import ugettext as _, ugettext_lazy as l_

def get_menu_path(*k,**kw):
    from geo.server import get_config
    menu_path = get_config('menu_function')
    if menu_path: 
        menu = menu_path
    else:   
        menu = {"href":"/admin/function", "label":_("Function")}
    return menu

class FunctionController(DefaultModelController):
    """
        When user clicks in user_help a pop up appears so the user can put all info there.
    """
    __list_columns__ = ["constant", "description"]
    __model_object__ = Function
    __columns_name__ = [l_("Constant"), 
                        l_("Description")]
    __edit_columns_names__ =[l_("Constant"), 
                             l_("Description"), 
                             l_("Area Function"), 
                             l_("User help keys")]
    __edit_columns__ = ["constant", "description", "area_function", "user_help"]
    __title__ = l_("List of functions")
    __form_title__ = l_("Function")
    __menu__=get_menu_path
    __schema_validator__ = FunctionForm

    
    @cherrypy.expose
    @template.output('form.html', geo_template=True)
    def request(self, id=None, cancel=False, **kw):
        if callable(self.__menu__):
            self.__menu__ = self.__menu__.__call__()
        errors, form_data, title, success = self.prepare_submit_data(id, cancel, **kw)
        setHelpStr = _("\'Set help text\'")
        user_help = MarkupTemplate('<input id="user_help" name="user_help" onfocus="modal_cleditor(this, %(help_text)s)"></input>'%{"help_text": setHelpStr}) 
        user_filler = html.HTMLFiller(data=form_data)
        user_help = (user_help.generate() | user_filler)
        if id:
            disabled = 'disabled="disabled"'
        else:
            disabled = ''
        area_function = MarkupTemplate('<input class="checkbox" id="area_function" name="area_function" %(disabled)s type="checkbox" ></input>' %{"disabled": disabled})
        area_filler = html.HTMLFiller(data=form_data)
        area_function = (area_function.generate() | area_filler)
        return template.render(errors=errors, columns_names= self.__edit_columns_names__,
                               edit_columns=self.__edit_columns__, title=title,
                               menu=self.__menu__, success=success,
                               user_help=user_help, area_function = area_function,
                               insert_javascript=['/media/cleditor/jquery.cleditor.min.js',
                                                  '/media/js/modal_cleditor.js'],
                               insert_css=['/media/cleditor/jquery.cleditor.css'],
                               geo_template=True) | HTMLFormFiller(data=form_data)
                               
                               
    def edit(self, id, kw):
        """
            In edit user can not set is_area_function attribute, so we can not use
            the method edit from DefaultModelController
        """
        object_id =id
        session = cherrypy.request.db
        update_object = session.query(self.__model_object__).filter(getattr(self.__model_object__, "id")==object_id).first()
        for key, value in kw.iteritems():
            if key != "id":
                setattr(update_object, key, value)
        session.flush()
        
    @cherrypy.expose
    def delete(self, object_id):
        redirect_path = self.get_controller_path("delete")
        session = cherrypy.request.db
        
        function_object = session.query(Function).filter(Function.id==object_id).first()
        function_object.device_point_functions=[]
        function_object.device_area_functions=[]
        session.delete(function_object)
        raise cherrypy.HTTPRedirect(redirect_path)
  