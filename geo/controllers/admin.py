'''
Created on Feb 1, 2012

@author: newton
'''
import cherrypy
from geo.lib import template
from geo.controllers import DeviceController, FunctionController, \
                DevicePointFunctionController, DeviceAreaFunctionController
from geo.tools.i18n_tool import ugettext as _

class AdminController:
    """
        This controller should not be exposed if you do not want
        the user setting relevant configurations
    """
    device = DeviceController()
    function = FunctionController()
    device_point_function = DevicePointFunctionController()
    device_area_function = DeviceAreaFunctionController()
    
    @cherrypy.expose
    @template.output('admin.html', geo_template=True)
    def index(self):
        return template.render(title=_("Administrative Interface"), geo_template=True)