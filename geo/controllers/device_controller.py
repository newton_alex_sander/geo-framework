import cherrypy

from genshi import HTML
from genshi.template import MarkupTemplate
from genshi.filters import HTMLFormFiller
from formencode import Invalid
from sqlalchemy.exc import IntegrityError

from geo.lib import template, html
from geo.database.model import DevicePointFunction, Device, Function
from default_controller import DefaultModelController
from geo.validation.form import DeviceForm
from geo.tools.i18n_tool import ugettext as _, ugettext_lazy as l_

def get_menu_path(*k,**kw):
    from geo.server import get_config
    menu_path = get_config('menu_device')
    if menu_path: 
        menu = menu_path
    else:   
        menu = {"href":"/admin/device", "label":_("Device")}
    return menu

class DeviceController(DefaultModelController):
    __list_columns__ = ["device_id", "description", "point_functions_applied", "area_functions_applied"]
    __model_object__ = Device
    __columns_name__ = [l_("Device"), 
                        l_("Description"), 
                        l_("Point Functions Applied"), 
                        l_("Area Functions Applied")]
    __edit_columns__ = ["device_id", "description"]
    __edit_columns_names__ = [l_("Device"), 
                              l_("Description")]
    __menu__ = get_menu_path
    __title__ = l_("List of devices")
    __form_title__ = l_("Device")
    __schema_validator__ = DeviceForm


    @cherrypy.expose
    @template.output('table.html', geo_template=True)
    def index(self):
        session = cherrypy.request.db
        query = session.query(self.__model_object__)
        items = query.all()
        point_functions_applied=[]
        area_functions_applied=[]
        for device in items:

            if len(device.point_functions_applied)>0:
                points = [point.constant for point in device.point_functions_applied]
                formatted = ", ".join(points)
                point_functions_applied.append(formatted)
            else:
                point_functions_applied.append(device.point_functions_applied)

            if len(device.area_functions_applied)>0:
                areas = [area.constant for area in device.area_functions_applied]
                formatted = ", ".join(areas)
                area_functions_applied.append(formatted)
            else:
                area_functions_applied.append(device.area_functions_applied)

            
        return template.render(items=items,
                               point_functions_applied=point_functions_applied,
                               area_functions_applied=area_functions_applied,  
                               list_columns=self.__list_columns__, 
                               columns_names=self.__columns_name__, 
                               title=self.__title__,
                               form_title=self.__form_title__,
                               geo_template=True)

    def edit(self, id, kw):
        session = cherrypy.request.db
        device_object = session.query(Device).filter(Device.id==id).first()
        setattr(device_object, "device_id", kw["device_id"])
        setattr(device_object, "description", kw["description"])
        session.flush()

    
    @cherrypy.expose
    def delete(self, object_id):
        redirect_path = self.get_controller_path("delete")
        session = cherrypy.request.db
        
        device_object = session.query(Device).filter(Device.id==object_id).first()
        device_object.point_functions_applied=[]
        device_object.area_functions_applied=[]
        session.delete(device_object)
        raise cherrypy.HTTPRedirect(redirect_path)
