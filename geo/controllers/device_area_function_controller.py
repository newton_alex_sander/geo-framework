import cherrypy
from geo.lib import template
from geo.database.model import DeviceAreaFunction, Device, Function, Area
from geo.tools.i18n_tool import ugettext as _, ugettext_lazy as l_

class DeviceAreaFunctionController():
    __model_object__ = DeviceAreaFunction
    __title__ = l_("Set area function parameter")
    
    
    @cherrypy.expose
    @template.output('device_area_function.html', geo_template=True)
    def index(self):
        session = cherrypy.request.db
        devices = session.query(Device).all()
        functions = session.query(Function).filter(Function.area_function==True).all()
        areas= session.query(Area).all()
        device_area_functions= session.query(DeviceAreaFunction).all()
        return template.render(device_area_functions=device_area_functions,
                               devices=devices, 
                               functions=functions, 
                               areas=areas, 
                               title=self.__title__, 
                               insert_javascript=['/media/js/device_function.js',
                                                  '/media/js/jquery.nestedAccordion.js',
                                                  '/media/js/expand.js',
                                                  '/media/js/modal_text.js'],
                                insert_css=['/media/css/function_parameter.css'],
                                geo_template=True)

    
    @cherrypy.expose
    def get_config(self, device_id, function_id, area_id):
        session = cherrypy.request.db
        query = session.query(DeviceAreaFunction).filter_by(device_id=device_id).filter_by(function_id=function_id).filter_by(area_id=area_id)
        try:
            device_function = query.one()
        except:
            return ""

        if device_function.parameters ==None:
            return ""
        else:
            return_string = ""
            for key, value in device_function.parameters.iteritems():
                return_string+="%s=%s;" %(key, value) 
            return return_string
            
            
    @cherrypy.expose
    def set_config(self, device_id, function_id, area_id, config):
        session = cherrypy.request.db
        
        query = session.query(DeviceAreaFunction).filter_by(device_id=device_id).filter_by(function_id=function_id).filter_by(area_id=area_id)
        if query.count()!=0:
            try:
                device_function = query.one()
            except:
                return _("Error")
        else:
            try:
                device_object = session.query(Device).filter(Device.id==device_id).first()
                function_object = session.query(Function).filter(Function.id==function_id).first()
                area_object = session.query(Area).filter(Area.id==area_id).first()
                if device_object ==None:
                    return _("Error")
                if area_object ==None:
                    return _("Select one area")
                if function_object ==None:
                    return _("Select one function")
                device_function = DeviceAreaFunction(function_object, device_object, area_object)
                session.add(device_function)
                session.flush()
                session.commit()
            except Exception, e:
                return str(e)
        try:
            parameters_dict = {}
            items = str(config).split(";")
            for item in items:
                if str(item).count("=")>0:
                    key_value = str(item).split("=")
                    key = key_value[0]
                    value = key_value[1]
                    parameters_dict[key] = value
            device_function.parameters =parameters_dict
            session.flush()
            session.commit()
            return _("Saved!")
        except Exception, e:
            return str(e)
            
    @cherrypy.expose
    def remove(self, device_id, area_id, function_id):
        session = cherrypy.request.db
        try:
            device_area=session.query(DeviceAreaFunction).filter(DeviceAreaFunction.device_id==device_id).filter(DeviceAreaFunction.function_id==function_id).filter(DeviceAreaFunction.area_id==area_id).first()
            device_area.area_events=[]
            session.delete(device_area)
            return True
        except:
            return _("Problem removing")
    
    
    @cherrypy.expose
    def get_help(self, function_id):
        session = cherrypy.request.db
        function = session.query(Function).get(function_id)
        if function==None:
            return _("Select another function")
        else:
            if str(function.user_help)=="" or function.user_help==None:
                return _("There is no user help recorded")
            else:
                return str(function.user_help)