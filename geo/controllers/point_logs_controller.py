'''
Created on Feb 1, 2012

@author: newton
'''
import cherrypy
from geo.lib import template
from geo.database.model import PointEvent
from geo.tools.i18n_tool import ugettext as _, ugettext_lazy as l_
from sqlalchemy.sql.expression import desc
from math import ceil

class PointLogsController:
    """
        This controller shows a jqgrid component with all point events from database
    """
    @cherrypy.expose
    @template.output('jqgrid.html', geo_template=True)
    def index(self):
        return template.render(title=l_("Point events"),
                               col_names = str(['event_id',
                                                _('Device').encode("latin1"),
                                                _('Event type').encode("latin1"), 
                                                _('Date Started').encode("latin1"), 
                                                _('Date Stopped').encode("latin1"),
                                                _('Active').encode("latin1"),
                                                _('Message').encode("latin1")]),
                               col_model = """[{name:'id',index:'id', hidden:true}, 
                                                {name:'device',index:'device', sortable:false},
                                                {name:'type', index:'type',sortable:false},
                                                {name:'started',index:'started', width:80, sortable:false, align:'center'},
                                                {name:'stopped',index:'stopped', width:80, sortable:false, align:'center'},
                                                {name:'active',index:'active', width:80, sortable:false, align:'center'},
                                                {name:'message',index:'message', sortable:false}
                                                ]
                                            """,
                               insert_javascript=['/media/js/jquery.jqGrid.min.js',
                                                  '/media/js/jquery.tablednd.js',
                                                  '/media/js/jquery.contextmenu.js',
                                                  '/media/js/i18n/grid.locale-en.js',
                                                  ],
                               insert_css=['/media/css/ui.jqgrid.css',], 
                               geo_template=True)

                             
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def fetch(self, *k ,**kw):
        offset = (int(kw['page']) - 1) * int(kw['rows'])
        session = cherrypy.request.db
        query = session.query(PointEvent)
        result_count=query.count()
        total = int(ceil(result_count / float(kw['rows'])))
        query = query.order_by(desc(PointEvent.event_stopped)).offset(offset).limit(kw['rows'])
        rows = []
        number_of_records = str(query.count())
        events = query.all()
        for event in events:
            event_stopped = _("Still active")
            if event.event_stopped!=None:
                event_stopped = event.event_stopped.strftime('%d/%m/%Y %H:%M:%S')
            row = dict(id=event.id, 
                        cell=[event.id,
                              unicode(event.device_point_func.device),
                              unicode(event.device_point_func.function.constant),
                              event.event_started.strftime('%d/%m/%Y %H:%M:%S'),
                              event_stopped,
                              unicode(event.active),
                              unicode(event.message)
                              ])
            rows.append(row)
        result = dict(page=kw['page'], 
                      total=total,
                      records=number_of_records,
                      rows=rows)
        return result
        
    