import cherrypy
from geo.lib import template
from geo.database.model import DevicePointFunction, Device, Function
from default_controller import DefaultModelController
from geo.tools.i18n_tool import ugettext as _, ugettext_lazy as l_

class DevicePointFunctionController(DefaultModelController):
    __model_object__ = DevicePointFunction
    __title__ = l_("Set point function parameter")
    @cherrypy.expose
    @template.output('device_point_function.html', geo_template=True)
    def index(self):
        session = cherrypy.request.db
        devices = session.query(Device).all()
        functions = session.query(Function).filter(Function.area_function==False).all()
        device_point_functions= session.query(DevicePointFunction).all()
        return template.render(device_point_functions=device_point_functions,
                               devices=devices, 
                               functions=functions, 
                               title=self.__title__, 
                               insert_javascript=['/media/js/device_function.js',
                                                  '/media/js/jquery.nestedAccordion.js',
                                                  '/media/js/expand.js',
                                                  '/media/js/modal_text.js'],
                               insert_css=['/media/css/function_parameter.css'],
                               geo_template=True)

    
  
    @cherrypy.expose
    def set_config(self, device_id, function_id, config):
        session = cherrypy.request.db
        query = session.query(DevicePointFunction).filter_by(device_id=device_id).filter_by(function_id=function_id)
        if query.count()!=0:
            try:
                device_function = query.one()
            except:
                return _("Error")
        else:
            try:
                device_object = session.query(Device).filter(Device.id==device_id).first()
                function_object = session.query(Function).filter(Function.id==function_id).first()
                if device_object ==None:
                    return _("Error")
                if function_object ==None:
                    return _("Select one function")
                
                device_function = DevicePointFunction(function_object, device_object)
                session.add(device_function)
                session.flush()
            except Exception, e:
                return str(e)
        
        try:
            parameters_dict = {}
            items = str(config).split(";")
            for item in items:
                if str(item).count("=")>0:
                    key_value = str(item).split("=")
                    key = key_value[0]
                    value = key_value[1]
                    parameters_dict[key] = value
            device_function.parameters =parameters_dict
            return _("Saved!")
        except Exception, e:
            return str(e)
    
    
    @cherrypy.expose
    def remove(self, device_id, function_id):
        session = cherrypy.request.db
        try:
            device_point = session.query(DevicePointFunction).filter(DevicePointFunction.device_id==device_id).filter(DevicePointFunction.function_id==function_id).first()
            device_point.point_events=[]
            session.delete(device_point)
            return True
        except Exception, e:
            return _("Problem removing %s" % e)
        
    
    @cherrypy.expose
    def get_help(self, function_id):
        session = cherrypy.request.db
        function = session.query(Function).get(function_id)
        if function==None:
            return _("Select another function")
        else:
            if str(function.user_help)=="" or function.user_help==None:
                return _("There is no user help recorded")
            else:
                return str(function.user_help)