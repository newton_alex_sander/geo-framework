'''
Created on Feb 1, 2012

@author: newton
'''
import cherrypy
from geo.lib import template
from shapely.wkb import loads
from geo.database import Device, Area, Point
from geo.database.model import AreaEvent, PointEvent
from geo.server import get_config
from geo.tools.i18n_tool import ugettext as _
show_areas_on_map = get_config('show_areas_on_map')

class MapsController:
    """
        Two important things about this controller:
            It exposes a map from GoogleMaps
            It sends data to the browser using HTML5 ServerSentEvents - www.w3.org/TR/eventsource/ 
    """
    @cherrypy.expose
    @template.output('maps.html', geo_template=True)
    def index(self):
        session = cherrypy.request.db
        session_id = cherrypy.session.id
        cherrypy.session["session_id"] = session_id
        queue = cherrypy.engine.serverDB.create_session_queue(session_id)

        devices = session.query(Device).all()
        
        if show_areas_on_map:
            areas = session.query(Area).all()
        else:
            areas =[]
        
        return template.render(title=_("GeoMap"), devices = devices, areas=areas, 
                               insert_javascript=['/media/js/jquery.multiselect.js',
                                                  '/media/js/jquery.ui.button.js',
                                                  '/media/js/jquery.ui.widget.js',
                                                  '/media/js/maps.js',
                                                  ],
                               insert_css=['/media/css/jquery.multiselect.css',], 
                               geo_template=True)


    @cherrypy.expose
    def device_data(self, _=None):
        """
            Event stream.
            Return objects from queues in event-stream format.
        """
        cherrypy.response.headers["Content-Type"] = "text/event-stream"
        session_id = cherrypy.session.get("session_id")
        queue = cherrypy.engine.serverDB.get_session_queue(session_id)
        session = cherrypy.request.db
        def content():
            if queue:
                while not queue.stream_queue_empty():
                    item = queue.get_item_from_queue()
                    if isinstance(item, dict):
                        data = "event: event\n"
                        data += 'data: '+ str(item).replace("'", '"')+'\n\n'
                        yield data
                    elif isinstance(item, Point):
                        device = session.query(Device).filter(Device.id==item.device_id).one()
                        data = "event: marker\n"
                        loaded_geo_point = loads(str(item.geo_point.geom_wkb))
                        marker ={"lat":str(loaded_geo_point.x), 
                                 "lon":str(loaded_geo_point.y), 
                                 "device_id":str(item.device_id),
                                 "device_description":str(device.description)}
                        data += 'data: '+ str(marker).replace("'", '"')+'\n\n'
                        yield data
        return content()
    device_data._cp_config = {'response.stream': True, 'tools.encode.encoding':'utf-8'}
    
    
    @cherrypy.expose
    def stop_streaming(self):
        session_id = cherrypy.session.get("session_id")
        queue = cherrypy.engine.serverDB.get_session_queue(session_id)
        if queue:
            queue.clear_listening_devices()
            return _("Stopped")
        else:
            return _("Session expired, refresh page")
    
    @cherrypy.expose
    def add_listener(self, device_id):
        session_id = cherrypy.session.get("session_id")
        queue = cherrypy.engine.serverDB.get_session_queue(session_id)
        if queue:
            queue.add_listener_device(device_id)
            return _("Added")
        else:
            return _("Session expired, refresh page")
    
    @cherrypy.expose
    def del_listener(self, device_id):
        session_id = cherrypy.session.get("session_id")
        queue = cherrypy.engine.serverDB.get_session_queue(session_id)
        if queue:
            del_listener = queue.del_listener_device(device_id)
            return _("Removed")
        else:
            return _("Session expired")

    @cherrypy.expose    
    @cherrypy.tools.json_out()
    def get_area(self, area_id):
        session = cherrypy.request.db
        area_object = None
        try:
            area_object = session.query(Area).filter(Area.id==area_id).first()
        except:
            return _("False")
        points = area_object.geom.coords(session)[0][:-1]
        return points

    @cherrypy.expose
    def event_info(self, item_id, item_type, *k ,**kw):
        """
            Pop up with event info
        """
        session = cherrypy.request.db
        device_description = ""
        if unicode(item_type).lower()=="area":
            event = session.query(AreaEvent).filter_by(id=item_id).one()
            device_description = event.device_area_func.device.description
        elif unicode(item_type).lower()=="point":
            event = session.query(PointEvent).filter_by(id=item_id).one()
            device_description = event.device_point_func.device.description
        else:
            return _("Problem getting event")
        
        stopped = _("Still active")
        if event.event_stopped!=None:
            stopped = event.event_stopped.strftime('%d/%m/%Y %H:%M:%S')
        device_str = _("Device:")
        event_start_str = _("Event started:")
        event_stop_str = _("Event stopped:")
        active_str = _("Active:")
        message_str = _("Message:")
        data =  """ 
                        <table style="background: #ddd">
                            <tr>
                                <td>
                                   %(device_str)s 
                                </td>
                                <td>
                                    %(device)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   %(event_start_str)s 
                                </td>
                                <td>
                                    %(event_started)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   %(event_stop_str)s
                                </td>
                                <td>
                                    %(event_stopped)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   %(active_str)s 
                                </td>
                                <td>
                                    %(active)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   %(message_str)s
                                </td>
                                <td>
                                    %(message)s
                                </td>
                            </tr>
                        </table>

                """ %{"device_str":device_str,
                      "event_start_str":event_start_str,
                      "event_stop_str":event_stop_str,
                      "active_str": active_str,
                      "message_str": message_str,
                      "device":device_description,
                      "event_started": event.event_started.strftime('%d/%m/%Y %H:%M:%S'), 
                      "event_stopped": stopped,
                      "active": event.active,
                      "message": event.message,}
        return data
        
    