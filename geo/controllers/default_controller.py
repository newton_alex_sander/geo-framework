'''
Created on Feb 2, 2012

@author: newton
'''

import cherrypy
from geo.lib import template
from formencode import Invalid
from genshi.filters import HTMLFormFiller
from geo.validation.form import DefaultValidator
from sqlalchemy.exc import IntegrityError
from geo.tools.i18n_tool import ugettext as _, ugettext_lazy as l_

class DefaultModelController:
    """
        This is a generic controller with all CRUD methods,
        You can always use it or extend it.
        The best way to understand how it works is looking to 
        source code of FunctionController or DeviceController
    """
    __list_columns__ = None # columns that will appear in index
    __edit_columns__ = None # fields that will be edited
    __model_object__ = None # sqlalchemy object
    __columns_name__ = None # labels of columns in index
    __edit_columns_names__ = None # labels of fields that will be edited
    __title__ = None # page title
    __menu__ = None # dict with label and href keys. Will appear in menu if set(only in create/edit pages)
    __schema_validator__ = None # formencode.Schema so user need to put valid values in forms
    
    def __init__(self):
        if self.__edit_columns_names__ == None:
            self.__edit_columns_names__ = self.__columns_name__
        if self.__edit_columns__ ==  None:
            self.__edit_columns__ = self.__list_columns__
        if self.__schema_validator__ == None:
            self.__schema_validator__ = DefaultValidator
    
    def get_controller_path(self, string_to_split):
        path =  cherrypy.request.path_info
        controller_path = path.rsplit(string_to_split)[0]
        return controller_path
    
    @cherrypy.expose
    @template.output('table.html', geo_template=True)
    def index(self):
        session = cherrypy.request.db
        query = session.query(self.__model_object__)
        items = query.all()
        return template.render(items=items, 
                               list_columns=self.__list_columns__, 
                               columns_names=self.__columns_name__, 
                               title=self.__title__, 
                               form_title=self.__form_title__,
                               geo_template=True)


    @cherrypy.expose
    def delete(self, object_id):
        redirect_path = self.get_controller_path("delete")
        session = cherrypy.request.db
        session.query(self.__model_object__).filter(getattr(self.__model_object__, "id")==object_id).delete()
        raise cherrypy.HTTPRedirect(redirect_path)
        
    
    def prepare_submit_data(self, id=None, cancel=False, **kw):
        errors={}
        form_data = None
        success=None
        title = ""
        redirect_path = self.get_controller_path("submit")
        if cancel:
            raise cherrypy.HTTPRedirect(redirect_path)
        
        if id!=None:
            """
                Treats update GET
            """
            title = _("Updating %s") % self.__form_title__
            form_data = self.update_get(id)
        else:
            """
                Treats create GET
            """
            title = _("Creating %s") % self.__form_title__
            form_data = kw
        
        if cherrypy.request.method == 'POST':
            form = self.__schema_validator__()
            try:
                form_data = form.to_python(kw)
                
                if id!=None:
                    """
                        Submit updating
                    """
                    self.edit(id, kw)
                    success =  _("%s updated") % self.__form_title__
                else:
                    """
                        Submit creating
                    """
                    self.add(**form_data)
                    success =  _("%s created") % self.__form_title__
            except Invalid, e:
                errors = e.unpack_errors()
            except IntegrityError, e:
                cherrypy.request.db.rollback()
                errors={'general_error':"Integrity Restriction Error"}
        return errors, form_data, title, success
        
    
    @cherrypy.expose
    @template.output('form.html', geo_template=True)
    def request(self, id=None, cancel=False, **kw):
        if callable(self.__menu__):
            self.__menu__ = self.__menu__.__call__()
        if hasattr(self, '__form_title__'):
            title = self.__form_title__
        errors, form_data, title, success = self.prepare_submit_data(id, cancel, **kw)
        return template.render(errors=errors, columns_names= self.__edit_columns_names__,
                               edit_columns=self.__edit_columns__, success=success,
                               title=title, menu=self.__menu__, geo_template=True) | HTMLFormFiller(data=form_data)
        
        
    def update_get(self, object_id):
        session = cherrypy.request.db
        update_object = session.query(self.__model_object__).filter(getattr(self.__model_object__, "id")==object_id).first()
        dict_object = {}
        for item in self.__edit_columns__:
            dict_object.update({item:getattr(update_object, item)})
        return dict_object
        
    def edit(self, id, kw):
        object_id =id

        session = cherrypy.request.db
        update_object = session.query(self.__model_object__).filter(getattr(self.__model_object__, "id")==object_id).first()
        for key, value in kw.iteritems():
            if key != "id":
                setattr(update_object, key, value)
        session.flush()
        
    def add(self, **kw):
        session = cherrypy.request.db
        new_object = self.__model_object__(**kw)
        session.add(new_object)
        session.flush()