import os
import geo
import cherrypy
from genshi.core import Stream
from genshi.output import encode, get_serializer
from genshi.template import Context, TemplateLoader
from genshi.filters import Translator 
from geo.tools.i18n_tool import ugettext as _

def template_loaded(template):
    Translator(_).setup(template)
    
#loader to get files from /templates
loader = TemplateLoader(
    os.path.join(os.path.curdir, 'templates'),
    auto_reload=True, callback=template_loaded
)

#loader to get files from geo lib
geo_loader = TemplateLoader(
    os.path.join(geo.__path__[0], 'templates'),
    auto_reload=True, callback=template_loaded
)

def output(filename, method='html', encoding='utf-8', geo_template=False, **options):
    """Decorator for exposed methods to specify what template they should use
    for rendering, and which serialization method and options should be
    applied.
        **
            If geo_template == True, will look for files in geo lib.
            If geo_template == False, will look for files in /templates.
        **
    """
    def decorate(func):
        def wrapper(*args, **kwargs):
            if geo_template:
                cherrypy.thread_data.template = geo_loader.load(filename)
            else:
                cherrypy.thread_data.template = loader.load(filename)
            opt = options.copy()
            if method == 'html':
                opt.setdefault('doctype', 'html')
            serializer = get_serializer(method, **opt)
            stream = func(*args, **kwargs)
            if not isinstance(stream, Stream):
                return stream
            return encode(serializer(stream), method=serializer,
                          encoding=encoding)
        return wrapper
    return decorate

def render(geo_template=False,*args, **kwargs):
    """Function to render the given data to the template specified via the
    ``@output`` decorator.
        **
            If geo_template == True, will look for files in geo lib.
            If geo_template == False, will look for files in /templates.
        **
    """
    if args:
        assert len(args) == 1, \
            'Expected exactly one argument, but got %r' % (args,)
        if geo_template:
            template = geo_loader.load(args[0])
        else:    
            template = loader.load(args[0])
        template.filters.insert(0, Translator(_)) 
        
    else:
        template = cherrypy.thread_data.template
    ctxt = Context(url=cherrypy.url)
    ctxt.push(kwargs)
    return template.generate(ctxt)