from geo.geo_functions import *
from geo.server import get_config

# this code will get geo_functions_module set by the application that extends this framework
# it will put all methods that are in __init__.py __all__ list and will put in globals()
geo_functions_module = get_config('geo_functions_module')
if geo_functions_module:
    if hasattr(geo_functions_module, '__all__'):
        for attr in geo_functions_module.__all__:
            globals()[attr] = getattr (geo_functions_module, attr)


class GeoFunctionDispatcher(object):
    """
        Aorta artery of the framework.
        This is an object dispatcher, this class will turn into the class
        passed in __init__.
        If someone put a Function in database and this dispatcher did not get
        the Function Class, will raise an Exception. 
        So, be sure to import your geo_functions_module and to put you geo_functions in __all__
    """
    def __init__(self, class_name=None):
        if not class_name:
            return

        classes = globals()
        
        if not class_name in classes:
            raise Exception("%s not found in global scope" % class_name)
        _class = classes[class_name]
        if not type(_class) == type(self.__class__):
            raise Exception("%s is not a class" % class_name)

        self.__class__ = _class
