class GeoFunction(object):
    """
        Generic class that all geo_function need to extend
    """
    def apply_function(self, *k, **kw):
        assert NotImplementedError