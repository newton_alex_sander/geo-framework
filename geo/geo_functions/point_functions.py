import cherrypy
from geo.events.generated_event import *
from geoalchemy.postgis import pg_functions as functions
from geo_function import GeoFunction

class SPEED(GeoFunction):
    """
        Functions related to device point and speed:
        Current functions:
            * max 
                raise MaxSpeedEvent when speed passes parameters['max']
            * min 
                raise MinSpeedEvent when speed is bellow parameters['min']
    """

    def apply_function(self, point, parameters):
        """
            Calculate distance between last inserted point and current point from the same device.
            Calculate time difference between both points. 
            Speed = distance/time_difference
            Multiplies by 3.6 to get speed in km/h 
        """
        speed = None
        session = cherrypy.request.db
        if cherrypy.engine.serverDB.has_key(point.device_id, "last_point"):
            last_inserted_point = cherrypy.engine.serverDB.get_data(point.device_id, "last_point")
            last_inserted_point_geo = last_inserted_point.geo_point
            distance =  session.scalar(functions.distance(functions.transform(point.geo_point,2163), 
                                                          functions.transform(last_inserted_point_geo,2163)))
            time_difference = (point.device_timestamp-last_inserted_point.device_timestamp).seconds
            speed = distance/time_difference
            
            speed *= 3.6    # converting m/s to km/h

        if speed and parameters:
            if parameters.has_key('max') :
                max_speed = parameters.get('max')
                if speed>float(max_speed):
                    raise MaxSpeedEvent(speed=speed)
            if parameters.has_key('min'):
                min_speed = float(parameters.get('min'))
                if speed<min_speed:
                    raise MinSpeedEvent(speed=speed)
        return speed