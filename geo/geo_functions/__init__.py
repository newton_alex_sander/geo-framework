from point_functions import SPEED
from area_functions import INTERSECTS_AREA, NOT_INTERSECTS_AREA

__all__= ['SPEED', 'INTERSECTS_AREA', 'NOT_INTERSECTS_AREA']
