class ActiveEvents():
    """
        With this class we will always know if an event is active or not.
        It will record point and area events in dictionary where the keys
        are the devices ids.
    """
    active_area_events = {}
    active_point_events = {}
    
    def add(self, event_type, device_id, constant, event_id):
        if event_type == 'area':
            if not self.active_area_events.has_key(device_id):
                self.active_area_events[device_id]={}
            this_device_events = self.active_area_events.get(device_id)
            this_device_events[constant] = event_id
        elif event_type == 'point':
            if not self.active_point_events.has_key(device_id):
                self.active_point_events[device_id]={}
            this_device_events = self.active_point_events.get(device_id)
            this_device_events[constant] = event_id
        
    def event_exists(self, event_type, device_id, constant):
        if event_type =='area':
            if self.active_area_events.has_key(device_id):
                this_device_events = self.active_area_events.get(device_id)
                if this_device_events.has_key(constant):
                    return True
        elif event_type =='point':
            if self.active_point_events.has_key(device_id):
                this_device_events = self.active_point_events.get(device_id)
                if this_device_events.has_key(constant):
                    return True
        return False
        
    def pop_event(self, event_type, device_id, constant):
        if event_type == 'area':
            if self.active_area_events.has_key(device_id):
                this_device_events = self.active_area_events.get(device_id)
                if this_device_events.has_key(constant):
                    this_event_id = this_device_events.pop(constant)
                    return this_event_id
        elif event_type =='point':
            if self.active_point_events.has_key(device_id):
                this_device_events = self.active_point_events.get(device_id)
                if this_device_events.has_key(constant):
                    this_event_id = this_device_events.pop(constant)
                    return this_event_id