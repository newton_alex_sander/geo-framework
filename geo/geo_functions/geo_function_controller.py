'''
Created on Feb 13, 2012

@author: newton
'''
from geo_function_dispatcher import GeoFunctionDispatcher
from geo.events.generated_event import GenericEvent
from geo.database.model import AreaEvent, PointEvent
from geo.geo_functions.active_events import ActiveEvents
import cherrypy
from datetime import datetime


class GeoFunctionController():
    """
        Heart of the framework.
        Responsible for getting the point, verifying if its device has geo_functions attached.
        Dispatches GeoFunction passing point and manages generated events raised by them.
        Activate and Deactivate area and point events.
    """
    
    session = None
    active_events = ActiveEvents()
    
    
    def _deactivate_area_event(self, area_event_id, put_into_queue=True):
        """
            Sets AreaEvent.active and alerting to False.
            Sets AreaEvent.event_stopped to now.
        """ 
        area_event = self.session.query(AreaEvent).filter_by(id=area_event_id).one()
        setattr(area_event, 'active',False)
        setattr(area_event, 'alerting', False)
        setattr(area_event, 'event_stopped', datetime.now())
        try:
            self.session.commit()
            if put_into_queue:
                alert_data = {"id": str(area_event.id), "msg": str(area_event.device_area_func.function.constant), "type":"Area", "active":"false"}
                cherrypy.engine.serverDB.insert_into_queues(area_event.device_area_func.device.device_id, alert_data)
        except:
            print "Problem deactivating area event"
        
    def _deactivate_point_event(self, point_event_id, put_into_queue=True):
        """
            Sets PointEvent.active and alerting to False.
            Sets PointEvent.event_stopped to now.
        """ 
        point_event = self.session.query(PointEvent).filter_by(id=point_event_id).one()
        setattr(point_event, 'active',False)
        setattr(point_event, 'alerting', False)
        setattr(point_event, 'event_stopped', datetime.now())
        try:
            self.session.commit()
            if put_into_queue:
                alert_data = {"id": str(point_event.id), "msg": str(point_event.device_point_func.function.constant), "type":"Point", "active":"false"}
                cherrypy.engine.serverDB.insert_into_queues(point_event.device_point_func.device.device_id, alert_data)
        except:
            print "Problem deactivating point event"
        

    def _manipulate_area_event(self, device_area_func, area_Exception):
        """
            Insert event in database and into_queues 
        """
        insert = False
        this_device_id = str(device_area_func.device_id)
        this_function_constant = device_area_func.function.constant
        query = self.session.query(AreaEvent).filter_by(device_area_func_id=device_area_func.id).filter_by(active=True)
        if query.count()==0:
            insert=True
        else:
            area_event = query.one()
            if not self.active_events.event_exists('area', this_device_id, this_function_constant):
                # this event is active on DB but not in the system. We will not put this into queue
                self._deactivate_area_event(area_event.id, False)
                insert=True
        if insert:
            try:
                area_event = AreaEvent(device_area_func, area_Exception.message)
                self.session.add(area_event)
                self.session.flush()
                
                area_event_id =area_event.id

                self.active_events.add('area', this_device_id, this_function_constant, area_event_id)
            except:
                pass
        alerting = self.session.query(AreaEvent).filter_by(device_area_func_id=device_area_func.id).filter_by(alerting=True)
        if insert or alerting.count()>0:
            alert_obj = alerting.first()
            alert_data = {"id": str(alert_obj.id), "msg": str(alert_obj.device_area_func.function.constant), "type":"Area", "active":"true"}
            cherrypy.engine.serverDB.insert_into_queues(device_area_func.device.device_id, alert_data)

    def _manipulate_point_event(self, device_point_func, point_Exception):
        """
            Insert event in database and into_queues 
        """
        insert = False
        this_device_id = str(device_point_func.device_id)
        this_function_constant = device_point_func.function.constant
        query = self.session.query(PointEvent).filter_by(device_point_func_id=device_point_func.id).filter_by(active=True)
        if query.count()==0:
            insert=True
        else:
            point_event = query.one()
            if not self.active_events.event_exists('point', this_device_id, this_function_constant):
                # this event is active on DB but not in the system.
                self._deactivate_point_event(point_event.id, False)
                insert=True
                
        if insert:
            try:
                point_event = PointEvent(device_point_func, point_Exception.message)
                self.session.add(point_event)
                self.session.flush()

                point_event_id =point_event.id

                self.active_events.add('point', this_device_id, this_function_constant, point_event_id)
            except:
                pass
        alerting = self.session.query(PointEvent).filter_by(device_point_func_id=device_point_func.id).filter_by(alerting=True)
        if insert or alerting.count()>0:
            alert_obj = alerting.first()
            alert_data = {"id": str(alert_obj.id), "msg": str(alert_obj.device_point_func.function.constant), "type":"Point", "active":"true"}
            cherrypy.engine.serverDB.insert_into_queues(device_point_func.device.device_id, alert_data )
         
    
    def _dispatch_and_run_point_function(self, point, device_function):
        """
            Get function.constant and parameters from device_function
            When it calls GeoFunctionDispatcher passing the Constant, that class
            will turn into the right GeoFunction Class
            If no event occurs and there is an active event for this device_function,
            it is time do deactivate this event(cause its not active anymore)
        """
        constant = device_function.function.constant
        parameters= device_function.parameters
        attachedFunction = GeoFunctionDispatcher(constant)
        try:
            attachedFunction.apply_function(point, parameters)
        except GenericEvent, event:
            self._manipulate_point_event(device_function, event)
        else:
            this_device_id = str(device_function.device_id)
            if self.active_events.event_exists('point', this_device_id, constant):
                this_event_id = self.active_events.pop_event('point', this_device_id, constant)
                self._deactivate_point_event(this_event_id)

    def _dispatch_and_run_area_function(self, point, device_function):
        """
            Get function.constant and parameters from device_function
            When it calls GeoFunctionDispatcher passing the Constant, that class
            will turn into the right GeoFunction Class
            If no event occurs and there is an active event for this device_function,
            it is time do deactivate this event(cause its not active anymore)
        """
        constant = device_function.function.constant
        area = device_function.area
        parameters= device_function.parameters
        attachedFunction = GeoFunctionDispatcher(constant)
        try:
            attachedFunction.apply_function(point, parameters, area)
        except GenericEvent, event:
            self._manipulate_area_event(device_function, event)
        else:
            this_device_id = str(device_function.device_id)
            if self.active_events.event_exists('area', this_device_id, constant):
                this_event_id = self.active_events.pop_event('area', this_device_id, constant)
                self._deactivate_area_event(this_event_id)


    def verify_attached_functions(self, point):
        """
            If point device has attached function, will dispatch and run it 
        """
        self.session= cherrypy.request.db
        try:
            session.expunge(point)
        except:
            pass
        
        # dispatching point functions
        for device_function in point.device.device_point_functions:
            self._dispatch_and_run_point_function(point, device_function)
            
        #dispatching area functions
        for device_function in point.device.device_area_functions:
            self._dispatch_and_run_area_function(point, device_function)
                 
        cherrypy.engine.serverDB.set_data(point.device_id, "last_point", point)
        