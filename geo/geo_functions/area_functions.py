from geo_function import GeoFunction
from geoalchemy.postgis import pg_functions as functions
from geo.events.generated_event import *
import cherrypy

class INTERSECTS_AREA(GeoFunction):
    """
        Functions related to device point intersecting area:
        Current parameters:
            * intersects 
                raise Intersects
            * time_inside
                raise MaxTimeInside
            * max_speed (inside an area)
                raise MaxSpeedInside
            * min_speed (inside an area)
                raise MinSpeedInside
    """
    
    def apply_function(self, point, parameters, area):
        self.point=point
        self.parameters=parameters
        self.area= area
        self.server_db = cherrypy.engine.serverDB
        self.session = cherrypy.request.db
        if 'intersects' in parameters:
            if parameters['intersects'].upper()=="TRUE":
                self.intersects()
        elif 'time_inside' in parameters:
            try:
                time_inside = int(parameters['time_inside']) 
            except:
                time_inside = None
            if time_inside:
                self.time_inside()
        elif 'max_speed' in parameters:
            try:
                max_speed = float(parameters['max_speed']) 
            except:
                max_speed = None
            if max_speed:
                self.max_speed()
        elif 'min_speed' in parameters:
            try:
                min_speed = float(parameters['min_speed']) 
            except:
                min_speed = None
            if min_speed:
                self.min_speed()
                
    def intersects(self):
        """
            Raise Intersects when point is intersecting area
        """
        intersects =  self.session.scalar(functions.intersects(self.point.geo_point, self.area.geom))
        if intersects:
            raise Intersects(self.area.name)
        
    def time_inside(self):
        """
            Raise MaxTimeInside if the device stay more than 'time_inside'seconds inside area 
        """
        time_inside = int(self.parameters['time_inside'])
        intersects =  self.session.scalar(functions.intersects(self.point.geo_point, self.area.geom))
        if intersects:
            first_time_inside = self.server_db.get_data(self.point.device_id, "time_inside")
            if not first_time_inside:
                self.server_db.set_data(self.point.device_id, "time_inside", (self.area.id, self.point.device_timestamp))
            else:
                if first_time_inside[0]==self.area.id:
                    time_first_intersect = first_time_inside[1]
                    if self.point.device_timestamp>time_first_intersect:
                        difference_between_time = self.point.device_timestamp-time_first_intersect
                        if difference_between_time.seconds>time_inside:
                            raise MaxTimeInside(self.area.name,str(time_inside), str(difference_between_time.seconds))
        else:
            first_time_inside = self.server_db.get_data(self.point.device_id, "time_inside")
            if first_time_inside:
                # device was not for enough time intersecting area, so, we need to delete 'time_inside'
                if first_time_inside[0]==self.area.id:
                    self.server_db.remove_key(self.point.device_id, "time_inside")
                    
    def max_speed(self):
        """
            Raise MaxSpeedInside if the device get over 'max_speed' inside area 
        """
        speed_limit = float(self.parameters['max_speed']) 
        intersects =  self.session.scalar(functions.intersects(self.point.geo_point, self.area.geom))
        if intersects:
            last_point = self.server_db.get_data(self.point.device_id, "max_speed")
            if last_point:
                if last_point[0]==self.area.id:
                    last_point_object = last_point[1]
                    last_inserted_point_geo = last_point_object.geo_point
                    distance =  self.session.scalar(functions.distance(functions.transform(self.point.geo_point,2163), 
                                                  functions.transform(last_inserted_point_geo,2163)))
                    time_difference = (self.point.device_timestamp-last_point_object.device_timestamp).seconds
                    speed = distance/time_difference
                    speed *= 3.6
                    if speed>speed_limit:
                        raise MaxSpeedInside(self.area.name, str(speed_limit), str(speed))
            self.server_db.set_data(self.point.device_id, "max_speed", (self.area.id, self.point))
        else:
            last_point = self.server_db.get_data(self.point.device_id, "max_speed")
            if last_point:
                if last_point[0]==self.area.id:
                    self.server_db.remove_key(self.point.device_id, "max_speed")
    
    def min_speed(self):
        """
            Raise MinSpeedInside if the device is above 'min_speed' inside area 
        """
        speed_limit = float(self.parameters['min_speed'])
        intersects =  self.session.scalar(functions.intersects(self.point.geo_point, self.area.geom))
        if intersects:
            last_point = self.server_db.get_data(self.point.device_id, "min_speed")
            if last_point:
                if last_point[0]==self.area.id:
                    last_point_object = last_point[1]
                    last_inserted_point_geo = last_point_object.geo_point
                    distance =  self.session.scalar(functions.distance(functions.transform(self.point.geo_point,2163), 
                                                  functions.transform(last_inserted_point_geo,2163)))
                    time_difference = (self.point.device_timestamp-last_point_object.device_timestamp).seconds
                    speed = distance/time_difference
                    speed *= 3.6
                    if speed<speed_limit:
                        raise MinSpeedInside(self.area.name, str(speed_limit), str(speed))
            self.server_db.set_data(self.point.device_id, "min_speed", (self.area.id, self.point))
        else:
            last_point = self.server_db.get_data(self.point.device_id, "min_speed")
            if last_point:
                if last_point[0]==self.area.id:
                    self.server_db.remove_key(self.point.device_id, "min_speed")
    

class NOT_INTERSECTS_AREA(GeoFunction):
    """
        Functions related to device point not intersecting areas:
        Current functions:
            * not_intersects
                raise NotIntersects
            * time_outside
                raise MaxTimeOutside
    """
    def apply_function(self, point, parameters, area):
        self.point=point
        self.parameters=parameters
        self.area= area
        self.server_db = cherrypy.engine.serverDB
        self.session = cherrypy.request.db
        if 'not_intersects' in parameters:
            if parameters['not_intersects'].upper()=="TRUE":
                self.not_intersects()
        elif 'time_outside' in parameters:
            try:
                time_outside = int(parameters['time_outside']) 
            except:
                time_outside = None
            if time_outside:
                self.time_outside()
                
    def not_intersects(self):
        """
            Raise NotIntersects when point is not intersecting area
        """
        intersects =  self.session.scalar(functions.intersects(self.point.geo_point, self.area.geom))
        if not intersects:
            raise NotIntersects(self.area.name)
    
    def time_outside(self):
        """
            Raise MaxTimeOutside when point is not intersecting area for more than parameters['time_outside'] seconds
        """
        time_outside = int(self.parameters['time_outside']) 
        intersects =  self.session.scalar(functions.intersects(self.point.geo_point, self.area.geom))
        if not intersects:
            first_time_outside = self.server_db.get_data(self.point.device_id, "time_outside")
            if not first_time_outside:
                self.server_db.set_data(self.point.device_id, "time_outside", (self.area.id, self.point.device_timestamp))
            else:
                if first_time_outside[0]==self.area.id:
                    time_first_intersect = first_time_outside[1]
                    if self.point.device_timestamp>time_first_intersect:
                        difference_between_time = self.point.device_timestamp-time_first_intersect
                        if difference_between_time.seconds>time_outside:
                            raise MaxTimeOutside(self.area.name,str(time_outside), str(difference_between_time.seconds))
        else:
            first_time_outside = self.server_db.get_data(self.point.device_id, "time_outside")
            if first_time_outside:
                # device was not for enough time intersecting area, so, we need to delete 'time_outside'
                if first_time_outside[0]==self.area.id:
                    self.server_db.remove_key(self.point.device_id, "time_outside")
    