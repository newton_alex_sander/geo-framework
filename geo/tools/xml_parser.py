import os
from xml.dom import minidom
from restclient import rest_invoke
from minixsv import pyxsval
from geo.server import get_config, set_framework_config
import geo


def insert_xml_data(config, xml_file):
    """
        Open xml_file and invoke rest for each point
    """
    set_framework_config(config)
    server_ip_address = get_config('server_ip_address')
    server_http_port = get_config('server_http_port')
    if not os.path.exists(xml_file):
        print xml_file + " could not be found"
        print "command: insert_xml_data xml_filename.xml"
    else:
        base_url = "http://%s:%s/rest" % (server_ip_address, server_http_port)
    
        error = False
        xsdpath = os.path.join(geo.__path__[0], 'testers', 'points.xsd')
        try:
            pyxsval.parseAndValidate (xml_file, 
                                      xsdFile=xsdpath,
                                      xmlIfClass= pyxsval.XMLIF_ELEMENTTREE,
                                      warningProc=pyxsval.PRINT_WARNINGS,
                                      errorLimit=200, verbose=1,
                                      useCaching=0, processXInclude=0)
        except pyxsval.XsvalError, errstr:
            print errstr
            print "Validation aborted!"
            error = True
           
        except Exception, e:
            print "Error"
            error = True

    
    
    
        if not error:
            print "Validation was successful!"
            xmldoc = minidom.parse(xml_file)
            points = xmldoc.getElementsByTagName("point")
            my_device = xmldoc.getElementsByTagName("points")[0]
            device_id = my_device.attributes.get("deviceid").value
                
            for point in points:
                timestamp_xml_element = point.getElementsByTagName("timestamp")[0]
                timestamp_value=timestamp_xml_element.firstChild.data
                
                latitude_xml_element = point.getElementsByTagName("latitude")[0]
                latitude_value=latitude_xml_element.firstChild.data
                
                longitude_xml_element = point.getElementsByTagName("longitude")[0]
                longitude_value=longitude_xml_element.firstChild.data
                
                params = {"time":timestamp_value, "longi":longitude_value,"lati":latitude_value, "deviceid":device_id}
                try:
                    rest_invoke(base_url, method="POST",async=False, resp=False, params=params)
                except Exception, e:
                    print "Problem with rest request: %s " % e
                    break