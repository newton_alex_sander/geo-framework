from geo import config as default_config

framework_config = default_config
def get_config(item):
    """
        This method is used by the framework to get the config
        written in extended application
    """
    return getattr(framework_config, item, None)

def set_framework_config(config):
    """
        This will make possible to the framework to use the right config file 
    """
    global framework_config
    framework_config= config