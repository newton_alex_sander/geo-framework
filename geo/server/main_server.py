import os
import cherrypy
from cherrypy import tools
from geo.server import get_config, set_framework_config
from geo.tools.i18n_tool import I18nTool

def assign_tools():
    """
        Creates sqlalchemy session,
        Creates our online database 
    """
    from geo.database.sqlalchemy_tool import SAEnginePlugin, SATool
    from geo.database.online_db import ServerDB
    SAEnginePlugin(cherrypy.engine).subscribe()
    tools.db = SATool()
    tools.I18nTool = I18nTool() 
    cherrypy.engine.serverDB = ServerDB(cherrypy.engine)
    

def run_server(root=None, config=None):
    if config:
        set_framework_config(config)
    
    from rest_resource import RESTResource
    from geo.controllers.root import Root
    from geo.controllers import AdminController, MapsController, PointLogsController, AreaLogsController
    
    assign_tools()
    
    # If you don't pass one root it will use geo Root
    if not root:
        root = Root()
    
    server_http_port = get_config('server_http_port')
    server_ip_address = get_config('server_ip_address')
    show_admin_page = get_config('show_admin_page')
    show_map_page = get_config('show_map_page')
    show_log_point_events_page = get_config('show_log_point_events_page')
    show_log_area_events_page = get_config('show_log_area_events_page')
    rest_address = get_config('rest_address')

    if show_admin_page:
        setattr(root, 'admin', AdminController())
    if show_map_page:
        setattr(root, 'map', MapsController())
    if show_log_point_events_page:
        setattr(root, 'point_events', PointLogsController())
    if show_log_area_events_page:
        setattr(root, 'area_events', AreaLogsController())
    
    rest_service = RESTResource()
    
    cherrypy.config.update({
        'tools.encode.on': True, 
        'tools.encode.encoding': 'utf-8',
        'tools.decode.on': True,
        'tools.trailing_slash.on': True,
        'tools.staticdir.root': os.path.join(os.path.abspath(os.path.dirname(__file__)), ".."),
        'tools.sessions.on': True,
        'tools.I18nTool.on': True,
        'tools.I18nTool.default': 'en_US',
        'tools.I18nTool.mo_dir': 'i18n',
        'tools.I18nTool.domain': 'translate',
        'log.screen':True,
        'checker.on':False,
        'session_filter.on':True,
    })
    
    conf = {
        'global': {
            'server.socket_host': server_ip_address,
            'server.socket_port': server_http_port,
            'tools.db.on': True,
            'engine.serverDB.on' : True,
        },
        
        '/media': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'static'
        },
    }
    

    cherrypy.tree.mount(rest_service, rest_address, config={"/":{'request.dispatch': cherrypy.dispatch.MethodDispatcher()}})
    
    cherrypy.quickstart(root, '/', conf)

    
    