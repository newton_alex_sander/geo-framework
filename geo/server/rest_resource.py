import cherrypy
from geo.database.model import Device, Point
from datetime import datetime
from geo.geo_functions.geo_function_controller import GeoFunctionController 
from geo.server import get_config

insert_point_without_created_device = get_config('insert_point_without_created_device')


class RESTResource():
    """
        MethodDispatcher. Manipulates HTTP POST requests.
            If insert_point_without_created_device ==True:
                Will create a device for inserted point if it do not exist 
        Needed args: 
            longitude = longi
            latitude =  lati
            device_ime = deviceid - unique device identifier
            time = time 
    """
    exposed = True
    geoFunctionController = GeoFunctionController()
    
    def POST(self, **kwargs):
        session = cherrypy.request.db
        
        longitude = kwargs['longi']
        latitude =  kwargs['lati']
        device_ime = kwargs['deviceid']
        time = kwargs['time']
        device_datetime=datetime.fromtimestamp(float(time))
        
        query = session.query(Device).filter_by(device_id=device_ime)
        if query.count()>0:
            device = query.first()
        else:
            if insert_point_without_created_device:
                device = Device(device_id=device_ime, description=device_ime)
                session.add(device)
                session.commit()
            else:
                return
        self.insert_point(device, latitude, longitude, device_datetime)
        

    def insert_point(self, device, latitude, longitude, device_datetime):
        """
            Insert point in database, verify if there is geo_function to be applied
            and insert point in serverDB queue (so it can be shown in browser)
        """
        session = cherrypy.request.db
        point = Point(device, latitude, longitude, device_datetime)
        session.add(point)
        session.commit()
       
        self.geoFunctionController.verify_attached_functions(point)
        try:
            session.expunge(point)
        except:
            pass
        cherrypy.engine.serverDB.insert_into_queues(device.device_id, point )
