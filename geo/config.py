from geo.tools.i18n_tool import ugettext as _, ugettext_lazy as l_
#===============================================================================
# Admin menu
#===============================================================================
# If false, will not show /function, /device and controllers to set function parameters
show_admin_page = True
# If false, will not show /admin/device
show_device_page = True
# If false, will not show /admin/function
show_function_page = True
# If false, will not show /admin/show_point_functions_page
show_point_functions_page = True
# If false, will not show /admin/device_area_function
show_area_functions_page = True


#===============================================================================
# General menu
#===============================================================================
# If false will not show /map
show_map_page = True
# If false will not show /point_events
show_log_point_events_page = True
# If false will not show /area_events
show_log_area_events_page = True
# If false will not show areas multiselect above google map in /map
show_areas_on_map = True


#===============================================================================
# General menu info
#===============================================================================
# Attributes that appear in menu
menu_area_events={"label":l_("Area events"), "href":"/area_events"}
menu_point_events={"label":l_("Point events"), "href":"/point_events"}
menu_map = {"label":l_("Map"), "href":"/map"}


#===============================================================================
# Admin menu info
#===============================================================================
# Attributes that appear in admin dropMenu
menu_admin = {"label":l_("Administrative")}
menu_device = {"label":l_("Device"), "href":"/admin/device"}
menu_function = {"label":l_("Function"), "href":"/admin/function"}
menu_area_functions = {"label":l_("Area function parameters"), "href":"/admin/device_area_function"}
menu_point_functions = {"label":l_("Point function parameters"), "href":"/admin/device_point_function"}


#===============================================================================
# GENERAL CONFIG
#===============================================================================
# Database address. 'postgresql://user:password@localhost/database_name'
sqlalchemy_engine_address = 'postgresql://gis:gis@localhost/gis'
server_http_port = 8080
server_ip_address = 'localhost'

# The address that will manipulate HTTP POST messages sending points
rest_address = '/rest'

# If this option is True:
#    when server receives a HTTP POST message with a device_id that do not exists,
#    one device will be created with (description=device_id, device_id=device_id)
# If you do not want to insert points without a created device, set this to false.
insert_point_without_created_device = True

# Project name, will appear in menu
project_name = l_("GeoFramework")

# You need to put your geo_functions module in this variable, so it can be used
# by the framework. Example:
# import geo_functions 
# geo_functions_module=geo_functions 
geo_functions_module= None 

