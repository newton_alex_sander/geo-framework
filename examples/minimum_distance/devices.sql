--
-- PostgreSQL database dump
--

-- Started on 2012-09-04 10:07:46 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2704 (class 0 OID 0)
-- Dependencies: 148
-- Name: devices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_minimum
--

SELECT pg_catalog.setval('devices_id_seq', 2, true);


--
-- TOC entry 2701 (class 0 OID 19358)
-- Dependencies: 149
-- Data for Name: devices; Type: TABLE DATA; Schema: public; Owner: app_minimum
--

COPY devices (id, device_id, description) FROM stdin;
1	353655044528747	Luana Piovani
2	353655044528311	Dado Dollabela
\.


-- Completed on 2012-09-04 10:07:46 BRT

--
-- PostgreSQL database dump complete
--

