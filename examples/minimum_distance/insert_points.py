import time
from restclient import rest_invoke
from geo.server import get_config, set_framework_config
import config

def insert_points(config):
    """
        Insert points in Database without stop
    """
    set_framework_config(config)
    server_ip_address = get_config('server_ip_address')
    server_http_port = get_config('server_http_port')
    base_url = "http://%s:%s/rest" % (server_ip_address, server_http_port)
    
    base_lati = 48.97720000
    base_longi = 8.41250000
    increasing = 0.0001500
    counter =0
    
    while True:
        counter +=1
        if counter ==1:
            #inserting luana
            device_id = "353655044528747"
            base_lati = 48.97920000
            longi = base_longi.__str__()
            lati = base_lati.__str__()
            device_time = time.time().__str__()
            params = {"time":device_time, "longi":longi,"lati":lati, "deviceid":device_id}
            rest_invoke(base_url, method="POST",async=False, resp=True, params=params)
            device_id = "353655044528311"
            base_lati = 48.97120000
            base_longi = 8.41250000
            time.sleep(2)
        else:
            #inserting dado
            try:
                base_lati += increasing
                longi = base_longi.__str__()
                lati = base_lati.__str__()
                device_time = time.time().__str__()
                params = {"time":device_time, "longi":longi,"lati":lati, "deviceid":device_id}
                rest_invoke(base_url, method="POST",async=False, resp=True, params=params)
                time.sleep(2)
            except:
                time.sleep(2)
                print 'problem inserting point'

insert_points(config)