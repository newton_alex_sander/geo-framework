import cherrypy
from geo.events.generated_event import GenericEvent
from geoalchemy.postgis import pg_functions as functions
from geo.geo_functions.geo_function import GeoFunction
from geo.database.model import  Device, Point


class TooClose(GenericEvent):
    def __init__(self, distance):
        self.distance = distance
        self.message = "Too close: %s m" % self.distance
        
class InvalidParametersForFunction(GenericEvent):
    def __init__(self, message):
        self.message = message

class MINIMUM_DISTANCE(GeoFunction):
    
    def apply_function(self, point, parameters):
        if parameters.has_key('another_device_id') and parameters.has_key('min_distance'):
            session = cherrypy.request.db
            another_device_object = None
            min_distance = None
            try:
                min_distance = float(parameters["min_distance"])
            except:
                raise InvalidParametersForFunction("%s is not a valid distance value" %(parameters["min_distance"]))
            try:
                another_device_id = parameters["another_device_id"]
                another_device_object = session.query(Device).filter(Device.device_id==another_device_id).first()
            except:
                raise InvalidParametersForFunction("Could not find Device with this id %s" %(another_device_id))
            
            if another_device_object and min_distance:
                last_point_another_device = session.query(Point).filter(Point.device==another_device_object).order_by('device_timestamp DESC').first()
                distance =  session.scalar(functions.distance(functions.transform(last_point_another_device.geo_point,2163), 
                                                      functions.transform(point.geo_point,2163)))
                if float(distance) < min_distance:
                    raise TooClose('%s and %s are too close: %.2f, min distance was: %s' %(point.device.description, 
                                                                                         another_device_object.description,
                                                                                         distance,
                                                                                         min_distance))
                
