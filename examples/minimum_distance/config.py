import geo_functions

#admin menu
show_admin_page = True
show_device_page = True
show_function_page = True
show_point_functions_page = True
show_area_functions_page = False

#general menu
show_map_page = True
show_log_point_events_page = True
show_log_area_events_page = False
show_areas_on_map = False

#general menu info
menu_area_events={"label":"Area events", "href":"/area_events"}
menu_point_events={"label":"Point events", "href":"/point_events"}
menu_map = {"label":"Map", "href":"/map"}

#admin menu info
menu_admin = {"label":"Administrative"}
menu_device = {"label":"Device", "href":"/admin/device"}
menu_function = {"label":"Function", "href":"/admin/function"}
menu_area_functions = {"label":"Area function parameters", "href":"/admin/device_area_function"}
menu_point_functions = {"label":"Point function parameters", "href":"/admin/device_point_function"}



sqlalchemy_engine_address = 'postgresql://app_minimum:app_minimum@localhost/app_minimum'
server_http_port = 8080
server_ip_address = 'localhost'

rest_address = '/rest'

insert_point_without_created_device = True
insert_created_geo_functions = True

project_name = "App Minimum Distance"
geo_functions_module= geo_functions
