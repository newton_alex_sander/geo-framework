'''
Created on Feb 1, 2012

@author: newton
'''
import cherrypy
from geo.lib import template
class Root:
    
    @cherrypy.expose
    @template.output('root.html', geo_template=True)
    def index(self):
        return template.render(title="Welcome to your Geo Application", geo_template=True)