import os
import geo
import shutil


def create_environ():
    files_tree = {'':['config.py'], 'controllers':['root.py']}
    os.mkdir("geo_functions")
    os.mkdir("controllers")
    os.mkdir("templates")
    
    init = open('geo_functions/__init__.py', 'w')
    init.write("__all__=[]")
    init.close()
    
    init = open('controllers/__init__.py', 'w')
    init.close()
    
    
    for path, files in files_tree.iteritems():
        for file in files:
            srcpath = os.path.join(geo.__path__[0], path, file)
            shutil.copy(srcpath, os.path.join(path, file))
    
    run_path = os.path.join(geo.__path__[0], 'tools', 'run.py')
    shutil.copy(run_path, 'run.py')
    
    
create_environ()