'''
Created on Feb 1, 2012

@author: newton
'''

from geo.server import main_server
from geo.testers.prepared_resources import create_database_data
from geo.testers.test_database import test_database_connection
from geo.testers.insert_area import insert_area
from geo.testers.insert_points import insert_points
from geo.tools.xml_parser import insert_xml_data
from controllers.root import Root
import sys
import config


def run():
    main_server.run_server(root=Root(), config=config)

message = """
Possible options are:
    - geo_functions: 
        insert SPEED, INTERSECTS and NOT_INTERSECTS 
        function definitions in Database  
        
    - test_database:
        only try to connect to database
        
    - insert_area:
        insert a predefined Area(polygon) in database
        
    - insert_points:
        insert points in Database without stop
        
    - insert_xml_data xml_file:
        manipulate xml file, insert points doing rest invokes
    
    - run(optional):
        run server 
"""
opts = sys.argv[1:]
if opts:
    if opts[0]=='geo_functions':
        create_database_data(config)
    elif opts[0]=='test_database':
        test_database_connection(config)
    elif opts[0]=='insert_area':
        insert_area(config)
    elif opts[0]=='insert_points':
        insert_points(config)
    elif opts[0]=='insert_xml_data':
        if len(opts)!=2:
            print 'you need to pass the xml file'
        else:
            insert_xml_data(config, opts[1])
    elif opts[0]=='run':
        run()
    else:
        print 'Wrong parameter',
        print message

else:
    run()