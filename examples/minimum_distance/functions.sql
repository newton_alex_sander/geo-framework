--
-- PostgreSQL database dump
--

-- Started on 2012-09-04 10:07:32 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2704 (class 0 OID 0)
-- Dependencies: 146
-- Name: functions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_minimum
--

SELECT pg_catalog.setval('functions_id_seq', 1, true);


--
-- TOC entry 2701 (class 0 OID 19345)
-- Dependencies: 147
-- Data for Name: functions; Type: TABLE DATA; Schema: public; Owner: app_minimum
--

COPY functions (id, constant, description, area_function, user_help) FROM stdin;
1	MINIMUM_DISTANCE	Generates event when on object is too close to another	f	<i><b>another_device_id</b></i> = The device id that this device is avoiding.<br><b><i>min_distance</i></b> = The minimum distance between both.<br>
\.


-- Completed on 2012-09-04 10:07:33 BRT

--
-- PostgreSQL database dump complete
--

