--
-- PostgreSQL database dump
--

-- Started on 2012-09-04 10:07:59 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2706 (class 0 OID 0)
-- Dependencies: 150
-- Name: device_point_functions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_minimum
--

SELECT pg_catalog.setval('device_point_functions_id_seq', 1, true);


--
-- TOC entry 2703 (class 0 OID 19371)
-- Dependencies: 151
-- Data for Name: device_point_functions; Type: TABLE DATA; Schema: public; Owner: app_minimum
--

COPY device_point_functions (id, function_id, device_id, parameters) FROM stdin;
1	1	2	\\200\\002}q\\001(U\\021another_device_idq\\002U\\017353655044528747q\\003U\\014min_distanceq\\004U\\0041300q\\005u.
\.


-- Completed on 2012-09-04 10:07:59 BRT

--
-- PostgreSQL database dump complete
--

