--
-- PostgreSQL database dump
--

-- Started on 2012-09-06 01:20:19 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2714 (class 0 OID 0)
-- Dependencies: 144
-- Name: areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_frota
--

SELECT pg_catalog.setval('areas_id_seq', 1, true);


--
-- TOC entry 2711 (class 0 OID 19001)
-- Dependencies: 145
-- Data for Name: areas; Type: TABLE DATA; Schema: public; Owner: app_frota
--

COPY areas (id, name, created, geom) FROM stdin;
1	Lake Karlsruhe	2012-09-06 00:38:34.007766	0103000020E61000000100000007000000A8C5E061DA7D48401C0934D8D4D92040C80BE9F0107E48404451A04FE4D92040B859BC58187E48404149810530DD20407847C66AF37D48407E8AE3C0ABDD20409BC6F65AD07D48403BA92F4B3BDD20405FCE6C57E87D484055849B8C2ADB2040A8C5E061DA7D48401C0934D8D4D92040
\.


-- Completed on 2012-09-06 01:20:20 BRT

--
-- PostgreSQL database dump complete
--

