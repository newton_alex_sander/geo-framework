--
-- PostgreSQL database dump
--

-- Started on 2012-09-06 00:53:32 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2710 (class 0 OID 0)
-- Dependencies: 148
-- Name: devices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_frota
--

SELECT pg_catalog.setval('devices_id_seq', 1, true);


--
-- TOC entry 2707 (class 0 OID 19032)
-- Dependencies: 149
-- Data for Name: devices; Type: TABLE DATA; Schema: public; Owner: app_frota
--

COPY devices (id, device_id, description) FROM stdin;
1	353655044528747	Caminhao azul
\.


-- Completed on 2012-09-06 00:53:32 BRT

--
-- PostgreSQL database dump complete
--

