--
-- PostgreSQL database dump
--

-- Started on 2012-09-06 00:52:43 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2712 (class 0 OID 0)
-- Dependencies: 150
-- Name: device_point_functions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_frota
--

SELECT pg_catalog.setval('device_point_functions_id_seq', 1, true);


--
-- TOC entry 2709 (class 0 OID 19045)
-- Dependencies: 151
-- Data for Name: device_point_functions; Type: TABLE DATA; Schema: public; Owner: app_frota
--

COPY device_point_functions (id, function_id, device_id, parameters) FROM stdin;
1	1	1	\\200\\002}q\\001U\\003maxq\\002U\\00250q\\003s.
\.


-- Completed on 2012-09-06 00:52:43 BRT

--
-- PostgreSQL database dump complete
--

