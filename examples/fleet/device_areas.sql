--
-- PostgreSQL database dump
--

-- Started on 2012-09-06 00:52:57 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2713 (class 0 OID 0)
-- Dependencies: 152
-- Name: device_area_functions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_frota
--

SELECT pg_catalog.setval('device_area_functions_id_seq', 1, true);


--
-- TOC entry 2710 (class 0 OID 19068)
-- Dependencies: 153
-- Data for Name: device_area_functions; Type: TABLE DATA; Schema: public; Owner: app_frota
--

COPY device_area_functions (id, function_id, device_id, area_id, parameters) FROM stdin;
1	2	1	1	\\200\\002}q\\001U\\012intersectsq\\002U\\004Trueq\\003s.
\.


-- Completed on 2012-09-06 00:52:57 BRT

--
-- PostgreSQL database dump complete
--

