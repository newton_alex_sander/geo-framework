--
-- PostgreSQL database dump
--

-- Started on 2012-09-06 00:17:54 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2710 (class 0 OID 0)
-- Dependencies: 146
-- Name: functions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: app_frota
--

SELECT pg_catalog.setval('functions_id_seq', 3, true);


--
-- TOC entry 2707 (class 0 OID 19019)
-- Dependencies: 147
-- Data for Name: functions; Type: TABLE DATA; Schema: public; Owner: app_frota
--

COPY functions (id, constant, description, area_function, user_help) FROM stdin;
1	SPEED	Speed verification	f	<b><i>max = </i></b>Event when speed exceed value(km/h). Ex.:<font color="#3366ff"><b>max=23;</b></font><br><b><i>min = </i></b>Event when speed is bellow value(km/h). Ex.:<font color="#3366ff"><b>min=50;</b></font><b><br></b>
2	INTERSECTS_AREA	Generates event when object is inside area	t	<i><b>intersects</b></i> = event when object enters area. Ex.:<font color="#3366ff"><b>intersects=True;</b></font><br><i><b>time_inside = </b></i>max<i><b> </b></i>value(seconds) inside area. Ex.:<font color="#3366ff"><b>time_inside=60;</b></font><br><i><b>max_speed =&nbsp;</b></i>max value(km/h) inside area. Ex.:<font color="#3366ff"><b>max_speed=80;</b></font><br><i><b>min_speed = </b></i>min value(km/h) inside area. Ex.:<font color="#3366ff"><b>min_speed=20;</b></font>
3	NOT_INTERSECTS_AREA	Generates event when object is not inside area	t	<i><b>not_</b></i><i><b>intersects</b></i> = event when object exits area. Ex.:<font color="#3366ff"><b>not_intersects=True;</b></font><br><i><b>time_outside = </b></i>max<i><b> </b></i>value(seconds) outside area. Ex.:<font color="#3366ff"><b>time_outside=60;</b></font><br><br><b></b>
\.


-- Completed on 2012-09-06 00:17:54 BRT

--
-- PostgreSQL database dump complete
--

