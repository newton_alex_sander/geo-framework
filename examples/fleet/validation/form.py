'''
Created on Feb 5, 2012

@author: newton
'''
from formencode import Schema, validators, FancyValidator, Invalid
from geo.validation.form import MyString


class TruckForm(Schema):
    allow_extra_fields = True
    device_id = validators.UnicodeString(not_empty=True)
    description = MyString(encoding='utf-8', not_empty=True)
    number_plate = validators.UnicodeString(not_empty=True)