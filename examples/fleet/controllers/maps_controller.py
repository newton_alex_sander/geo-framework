'''
Created on Feb 1, 2012

@author: newton
'''
import cherrypy
from geo.lib import template
from shapely.wkb import loads
from geo.database import Device, Area, Point
from geo.database.model import AreaEvent, PointEvent
from geo.controllers import MapsController

class MyMapsController(MapsController):
    @cherrypy.expose
    def event_info(self, item_id, item_type, *k ,**kw):
        session = cherrypy.request.db
        device_description = ""
        truck = ""
        if unicode(item_type).lower()=="area":
            event = session.query(AreaEvent).filter_by(id=item_id).one()
            device_description = event.device_area_func.device.description
            truck = event.device_area_func.device.truck
        elif unicode(item_type).lower()=="point":
            event = session.query(PointEvent).filter_by(id=item_id).one()
            device_description = event.device_point_func.device.description
            truck = event.device_area_func.device.truck
        else:
            return "Problem getting event"
        
        stopped = "Still active"
        if event.event_stopped!=None:
            stopped = event.event_stopped.strftime('%d/%m/%Y %H:%M:%S')
        data =  """ 
                        <table style="background: #ddd">
                            <tr>
                                <td>
                                   Device: 
                                </td>
                                <td>
                                    %(device)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Truck: 
                                </td>
                                <td>
                                    %(truck)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Event started: 
                                </td>
                                <td>
                                    %(event_started)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Event stopped:
                                </td>
                                <td>
                                    %(event_stopped)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Active: 
                                </td>
                                <td>
                                    %(active)s
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Message:
                                </td>
                                <td>
                                    %(message)s
                                </td>
                            </tr>
                        </table>

                """ %{"truck":truck,
                      "device":device_description,
                      "event_started": event.event_started.strftime('%d/%m/%Y %H:%M:%S'), 
                      "event_stopped": stopped,
                      "active": event.active,
                      "message": event.message,}
        return data
        
    