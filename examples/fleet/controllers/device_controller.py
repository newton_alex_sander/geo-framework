import cherrypy

from genshi.filters import HTMLFormFiller

from geo.lib import template, html
from database import Truck
from geo.database import Device
from geo.controllers.default_controller import DefaultModelController
from validation.form import TruckForm
from geo.controllers import FunctionController, \
                DevicePointFunctionController, DeviceAreaFunctionController
from maps_controller import MyMapsController


class DeviceController(DefaultModelController):
    __list_columns__ = ["device_id", 'truck', "description", "point_functions_applied", "area_functions_applied"]
    __model_object__ = Device
    __columns_name__ = ["Device", "Truck", "Description", "Point Functions Applied", "Area Functions Applied"]
    __edit_columns__ = ["device_id", "description", "number_plate", "driver"]
    __edit_columns_names__ = ["Device", "Description","Number Plate", "Driver"]
    __menu__ = {"href":"/", "label":"Device"}
    __title__ = "Truck"
    __schema_validator__ = TruckForm
    function = FunctionController()
    device_point_function = DevicePointFunctionController()
    device_area_function = DeviceAreaFunctionController()
    map = MyMapsController()
    
    
    @cherrypy.expose
    @template.output('table.html', geo_template=True)
    def index(self):
        session = cherrypy.request.db
        query = session.query(self.__model_object__)
        items = query.all()
        return template.render(items=items, list_columns=self.__list_columns__, columns_names=self.__columns_name__, title=self.__title__, geo_template=True)
    
    
    @cherrypy.expose
    @template.output('device_form.html')
    def request(self, id=None, cancel=False, **kw):
        errors, form_data, title, success = self.prepare_submit_data(id, cancel, **kw)
        return template.render(errors=errors, columns_names= self.__edit_columns_names__,
                               edit_columns=self.__edit_columns__, success=success,
                               title=title, menu=self.__menu__, geo_template=True) | HTMLFormFiller(data=form_data)
    
    def add(self, **kw):
        session = cherrypy.request.db
        new_device = Device(device_id=kw["device_id"], description=kw["description"])
        session.add(new_device)
        new_truck = Truck(number_plate=kw["number_plate"], device=new_device, driver=kw["driver"])
        session.add(new_truck)
        session.flush()


    def edit(self, id, kw):
        session = cherrypy.request.db
        device_object = session.query(Device).filter(Device.id==id).first()
        setattr(device_object, "device_id", kw["device_id"])
        setattr(device_object, "description", kw["description"])
        truck_object = session.query(Truck).filter(Truck.device_id==id).first()
        setattr(truck_object, "number_plate", kw["number_plate"])
        setattr(truck_object, "driver", kw["driver"])
        session.flush()

    
    @cherrypy.expose
    def delete(self, object_id):
        redirect_path = self.get_controller_path("delete")
        session = cherrypy.request.db
        
        truck_object = session.query(Truck).filter(Truck.device_id==object_id).first()
        session.delete(truck_object)
        
        device_object = session.query(Device).filter(Device.id==object_id).first()
        device_object.point_functions_applied=[]
        device_object.area_functions_applied=[]
        session.delete(device_object)
        raise cherrypy.HTTPRedirect(redirect_path)

    def update_get(self, object_id):
        session = cherrypy.request.db
        update_object = session.query(Device).filter(Device.id==object_id).first()
        dict_object = {}
        for item in ["device_id", "description"]:
            dict_object.update({item:getattr(update_object, item)})
        update_object = session.query(Truck).filter(Truck.device_id==object_id).first()
        for item in ["number_plate", "driver"]:
            dict_object.update({item:getattr(update_object, item)})
        return dict_object
        
