'''
Created on Feb 1, 2012

@author: newton
'''
import cherrypy
from geo.lib import template
from device_controller import DeviceController
from geo.controllers import FunctionController, \
                DevicePointFunctionController, DeviceAreaFunctionController
from maps_controller import MyMapsController
                
    
class Root:
    device = DeviceController()
    function = FunctionController()
    device_point_function = DevicePointFunctionController()
    device_area_function = DeviceAreaFunctionController()
    map = MyMapsController()
    @cherrypy.expose
    @template.output('root.html')
    def index(self):
        return template.render(title="Welcome to your Geo Application")