#admin menu
show_admin_page = True
show_device_page = True
show_function_page = True
show_point_functions_page = True
show_area_functions_page = True

#general menu
show_map_page = True
show_log_point_events_page = True
show_log_area_events_page = True
show_areas_on_map = True

#general menu info
menu_area_events={"label":"Truck area events", "href":"/area_events"}
menu_point_events={"label":"Truck speed events", "href":"/point_events"}
menu_map = {"label":"View trucks on map", "href":"/map"}

#admin menu info
menu_admin = {"label":"Administrative", "href":"/admin/"}
menu_device = {"label":"Device", "href":"/"}
menu_function = {"label":"Function", "href":"/function"}
menu_area_functions = {"label":"Area function parameters", "href":"/device_area_function"}
menu_point_functions = {"label":"Point function parameters", "href":"/device_point_function"}

sqlalchemy_engine_address = 'postgresql://app_frota:app_frota@localhost/app_frota'
server_http_port = 8080
server_ip_address = 'localhost'

rest_address = '/rest'

insert_point_without_created_device = True
insert_created_geo_functions = True



geo_functions_module= None ### module with extended geo_functions

project_name = "FleetControl"
