from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Unicode, String, DateTime, BOOLEAN, PickleType, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship, backref

from geo.database.model import Base
from geo.database.model import Device


class Truck(Base):
    __tablename__ = 'trucks'
    id = Column(Integer, primary_key=True)
    number_plate = Column(String, nullable=False, unique=True)
    driver = Column(String)
    
    device_id = Column(Integer, ForeignKey('devices.id'))
    device = relationship(Device, backref=backref("truck"))
    
    def __init__(self, number_plate, device, driver=""):
        self.number_plate = number_plate
        self.device = device
        self.driver = driver

    def __repr__(self):
        return "%s" % (self.number_plate)