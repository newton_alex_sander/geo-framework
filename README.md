# README #

This work had as goal the creation of a framework that identifies events through the analysis of spatio-temporal data of mobile objects and the development of applications that use the features provided by this framework. 

### Examples ###
There are two applications that use this framework extending its features on source package /Examples

### Installing ###
On a debian-like shell, run the following commands:

```
#!bash
sudo apt-get install postgresql postgresql-8.4-postgis python-setuptools python-dev libpq-dev python-psycopg2
su root
su postgres
psql -c "CREATE ROLE gisgroup;" 
createdb -E UNICODE template_postgis 
createlang -d template_postgis plpgsql

psql -d template_postgis < /usr/share/postgresql/8.4/contrib/postgis-1.5/postgis.sql
psql -d template_postgis < /usr/share/postgresql/8.4/contrib/postgis-1.5/spatial_ref_sys.sql psql -d template_postgis < /usr/share/postgresql/8.4/contrib/postgis_comments.sql

psql -c "ALTER TABLE geometry_columns OWNER TO gisgroup;" template_postgis 
psql -c "ALTER TABLE spatial_ref_sys OWNER TO gisgroup;" template_postgis 
psql -c "CREATE USER yourgisuser WITH PASSWORD 'yourpassword';"

psql -c "GRANT gisgroup TO yourgisuser;" 
createdb -T template_postgis -O yourgisuser your_database_name 
exit
exit

```
Extract the geo package and run the command in the extracted folder: 

```
#!bash
sudo python run.py install

```

Now the framework is installed.
If you want to run the application that comes with the framework you should edit the config.py file, setting the right values for sqlalchemy_engine_address and server_ip_address, the first variable is the address to connect to the database that you created before, the second is the ip address that the server will use to bind your application, if you do not know what to put in this variable, leave the value as it is by default.
After setting those two variables you can execute the command bellow:

```
#!bash

python run.py
```
If everything was right, you will be able to see the root page opening this url in your browser http://localhost:8080/.
If you had problems to see the page you can check your database connection with this command:

```
#!bash
python run.py test_database
```
If you want to use the geo_functions that are installed on the framework you can run the following command:

```
#!bash

python run.py geo_functions
```

### Extending ###

Obs.: The framework must already be installed.

Copy the file create_environ.py to your project folder. Run it with 'python create_environ.py'. Now you have a directory tree structure ready to start developing your application! To test if

everything was fine, edit the config.py file setting the right values for server_ip_address and sqlalchemy_engine_address. Run the server with the command python run.py.

If you saw the index page in your browser after tipping your server address, you can start the development of your application.
 
## Setting config options:

There are a lot of customization options in your config.py file, there is a comment in each variable that will help you understand what they are for.

## Creating new GeoFunctions:

Before start it's important that you know in which way your function will be called and which parameters will be passed.
When a new point comes in a HTTP POST Request, all area and point functions associated with the device that generated that point will be queried. But what is an area function or a point function and what are their features? Area and point functions are classes with a method that will try to generate an event with the data passed by parameter. The difference between Area and Point functions is that when you develop an Area Function you will receive the assigned area object in your method. And which parameters will be passed? In both area and point functions your method will receive the inserted point and the parameters to your function, in the very specific case of the area function, the area associated with the function will be passed too.

And what you do? You will create a subclass from GeoFunction and develop your own apply_function method, this class will be instantiated and apply_function will be called if there is a row in functions table with the exactly name of your class and if you assigned the function to a device, if you developed an area function, you will need to assign your function to an area too.

A very common thing in geo functions is the necessity to use more than one point (to calculate a device speed for example, you need at least two points to calculate the distance and time interval between both). How can you store points to do a buffer and use the last inserted points in your functions? There is a simple way to do this, there is a dictionary where you can place your data, the keys are the devices ids, by default, the last inserted point is inserted in this dictionary, anyway, it is always good to verify if the dictionary has your key, like this:

```
#!python
cherrypy.engine.serverDB.has_key(point.device_id, "last_point")

```
Getting and setting the data is very simple too:

```
#!python

cherrypy.engine.serverDB.get_data(point.device_id, "last_point") cherrypy.engine.serverDB.set_data(point.device_id, "last_point")
```
But how about your buffer? There is a very simple way to it too: 

```
#!python
cherrypy.engine.serverDB.set_data(point.device_id, "last_4_points", deque([point], 4))
```

Using a deque  http://docs.python.org/library/collections.html#collections.deque you can certificate yourself that if you append an object and the deque is full, the first inserted point will be popped and your new point will be the last one.
How about a full working example?



```
#!python

def apply_function(self, point, parameters):
	""" This function will calculate the speed, it uses 2 points """
	speed = None
	session = cherrypy.request.db
	serverDB = cherrypy.engine. serverDB
	if serverDB.has_key(point.device_id, "last_point"):
		last_inserted_point = serverDB.get_data(point.device_id, "last_point")
		last_inserted_point_geo = last_inserted_point.geo_point
		distance = session.scalar(
					functions.distance(
					functions.transform(point.geo_point, 2163),
					functions.transform(last_inserted_point_geo, 2163)
								)
						)
		last_point_timestamp = last_inserted_point.device_timestamp
		time_difference = (point.device_timestamp-last_point_timestamp).seconds
		speed = distance/time_difference
		speed *= 3.6
```

And what about the events when a condition is found? When you find your desire condition, like a speed above 40 km/h you will raise an Exception that will be a subclass from GenericEvent, if you raise another kind of Exception, it will not be treated. But if you have a different condition to generate an event to each device? Like, set a speed limit of 120km/h for a Ferrari and 70km/h for a Truck? This kind of customization is passed in the parameters variable of your apply_function method, each method has its own parameter for each function. So, the same Ferrari can generate an maximum_speed_event when it runs above 120km/h and a minimum_speed_event when it runs bellow 20km/h. Of course your truck will have a lower value for maximum_speed_event. In the following example you can see how to use the parameters values and how you raise an Event:


```
#!python
class MaxSpeedEvent(GenericEvent): 
	def __init__(self, speed):
		self.speed = speed
		self.message = "Exceeded the speed limit: %.2fkm/h" % speed

class MinSpeedEvent(GenericEvent): 
	def __init__(self, speed):
		self.speed = speed
		self.message = "Speed below the minimum: %.2fkm/h" % speed
	
	…
	
if speed and parameters:
	if parameters.has_key('max') : 
		max_speed = parameters.get('max') 
		if speed>float(max_speed):
			raise MaxSpeedEvent(speed=speed) 
	if parameters.has_key('min'):
		min_speed = float(parameters.get('min')) 
		if speed<min_speed:
			raise MinSpeedEvent(speed=speed)


```
Now that you created your events, your geo function class and that you inserted your function in the database it is time to plug your function to the framework. It is very simple, you just need to edit the file geo_functions/__init__.py, you need to import your geo_function and add to __all__ list.

After that you need to edit your config.py file adding this this import: import geo_functions and edit geo_functions_module variable to this:


```
#!python

geo_functions_module= geo_functions
```

After this, restart your server, your application is ready to receive points and apply your function.

## Changing the interface:

The easiest way to understand what to do is read the cherrypy documentation,  http://docs.cherrypy.org/stable/concepts/basics.html